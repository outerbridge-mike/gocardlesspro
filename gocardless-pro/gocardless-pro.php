<?php  
/* 
Plugin Name: GoCardless Pro 
Plugin URI: https://outerbridge.co.uk/
Description: Easily take GoCardless Pro payments through your WordPress site.
Author: Outerbridge
Author URI: https://outerbridge.co.uk
Version: 2.5.0
*/  

/**
 *
 * Changelog
 *
 *	v2.5.0	210625	Bitbucket release (was v2.3.3)
 *
 *	v2.3.3	180604	Updated the GC libraries to v1.7.0
 *	v2.3.2	180510	Updated the GC libraries to v1.6.0
 *	v2.3.1	180329	Updated the GC libraries to v1.5.1
 *	v2.3.0	180308	Tweaked the process flow on the rdf confirmation to avoid errors
 *	v2.2.0	180221	Updated the GC libraries to v1.5.0, allow subscriptions to be cancelled from the Subscriptions page, pre-fill first name, last name and email for logged in customers (thanks Ellen Schofield)
 *	v2.1.2	171221	Updated the GC libraries to v1.4.0
 *	v2.1.1	171201	Updated the GC libraries to v1.3.0
 *	v2.1.0	170920	Updated the GC libraries to v1.2.0 and added a help video tab to the Help & Guidance admin page
 *	v2.0.2	170524	Updated the GC libraries to v1.1.0
 *	v2.0.1	170511	Updated the GC libraries to v1.0.0
 *	v2.0.0	170331	Added Name Your Own Amount functionality and updated the GC libraries to v0.11.0
 *	v1.3.0	170311	Added HTTPS check and notification
 *	v1.2.2	170223	Updated the GC libraries
 *	v1.2.1	170216	Updated the GC libraries
 *	v1.2.0	161202	Updated the GC libraries, updated table altering process, added payment count (paytcount) to subscriptions, added a one-off payment option for subscriptions, added 2 new hooks for the one-off payment part of a subscription, updated the Help & Usage section, and some presentational changes
 *	v1.1.3	161014	Updated the GC libraries
 *	v1.1.2	160929	Updated the GC libraries
 *	v1.1.1	160914	Adjusted the init sequence to remove activation error
 *	v1.1	160825	Updated the GC libraries, and altered the initialisation process to reduce errors, especially checking PHP version first
 *	v1.0.1	160805	Updated Payment/Plans Set-Up page so that zero or negative amounts are handled more elegantly
 *	v1.0	160727	First version of the plugin, submitted to CodeCanyon
 *	v0.2	160723	Further internal workings
 *	v0.1	160406	Initial working version started
 *
 */

global $wpdb;
// define the table names to be used
global $obr_gcp_paylinks_table;
$obr_gcp_paylinks_table = "{$wpdb->prefix}obr_gcp_paylinks";
global $obr_gcp_redirect_flows_table;
$obr_gcp_redirect_flows_table = "{$wpdb->prefix}obr_gcp_redirect_flows";
global $obr_gcp_current_redirect_flows_table;
$obr_gcp_current_redirect_flows_table = "{$wpdb->prefix}obr_gcp_current_redirect_flows";
global $obr_gcp_payments_plans_table;
$obr_gcp_payments_plans_table = "{$wpdb->prefix}obr_gcp_payments_plans";

if (!class_exists("obr_gocardless_pro")){
	class obr_gocardless_pro{
		// version
		public $obr_gocardless_pro_version = '2.5.0';

		public $obr_gocardless_pro_system = false;
		
		function __construct(){
			// this hook will cause our creation function to run when the plugin is activated
			register_activation_hook(__FILE__, array($this, 'obr_plugin_init'));		
			register_deactivation_hook(__FILE__, array($this, 'obr_plugin_uninit'));		
			add_action('plugins_loaded', array($this, 'obr_plugin_update_check'));
			add_action('plugins_loaded', array($this, 'obr_gocardless_pro_initialise'));
			add_action('plugins_loaded', array($this, 'obr_internationalisation'));
			add_action('plugins_loaded', array($this, 'obr_wpsm'), 1);
			add_action('plugins_loaded', array($this, 'obr_load_classes'), 9999);
			add_action('wp_head', array($this, 'obr_header'));
			add_filter('the_content', array($this, 'obr_complete_redirect_flows'), 99);
			add_filter('wp_session_expiration', array($this, 'obr_wpsm_expiry'));
			add_action('admin_menu', array($this, 'obr_gocardless_pro_admin_menu'));
			add_action('admin_menu', array($this, 'obr_is_ssl_check'));
			// add shortcodes
			add_shortcode('gcp_paylink', array($this, 'obr_gcp_paylink'));
			add_shortcode('gcp_redirect_flow', array($this, 'obr_gcp_redirect_flow'));
			// initialisation
			add_action('wp_enqueue_scripts', array($this, 'obr_enqueue'), 999);
			add_action('admin_enqueue_scripts', array($this, 'obr_admin_enqueue'));
			// ajax
			add_action('wp_ajax_nopriv_gcp-ajax-submit', array($this, 'obr_gcp_ajax_submit'));
			add_action('wp_ajax_gcp-ajax-submit', array($this, 'obr_gcp_ajax_submit'));
			// cron
			register_activation_hook(__FILE__, array($this, 'obr_gcp_activation'));
			add_action('obr_gcp_hourly_event', array($this, 'obr_rdf_transients_cleanup'));
			register_deactivation_hook(__FILE__, array($this, 'obr_gcp_deactivation'));

		}
	
		function obr_plugin_init(){
			// check for updates
			$this->obr_plugin_update_check('init');
			// now add in a version number
			//add_option('obr_gocardless_pro_version', $this->obr_gocardless_pro_version);
		}

		function obr_plugin_uninit(){
			// remove version number
			delete_option('obr_gocardless_pro_version');
		}

		function obr_plugin_update_check($init = null){
			// check if there's an update
			$installed_ver = get_option("obr_gocardless_pro_version");
			if($installed_ver != $this->obr_gocardless_pro_version){

				global $wpdb;
				global $obr_gcp_paylinks_table;
				global $obr_gcp_redirect_flows_table;
				global $obr_gcp_current_redirect_flows_table;
				global $obr_gcp_payments_plans_table;

				$charset_collate = '';
				if (!empty($wpdb->charset)){
					$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
				}
				if (!empty($wpdb->collate)){
					$charset_collate .= " COLLATE $wpdb->collate";
				}

				$mysql = "CREATE TABLE $obr_gcp_paylinks_table (
					ref INT(11) NOT NULL AUTO_INCREMENT,
					paylink_link VARCHAR(100),
					paylink_text VARCHAR(100),
					text_colour VARCHAR(10),
					css TEXT,
					last_update TIMESTAMP,
					UNIQUE KEY ref (ref)
				) $charset_collate;";
				require_once(ABSPATH.'wp-admin/includes/upgrade.php');
				dbDelta($mysql);

				$mysql = "CREATE TABLE $obr_gcp_redirect_flows_table (
					ref INT(11) NOT NULL AUTO_INCREMENT,
					payment_plan INT(11) NOT NULL DEFAULT 0,
					success_redirect_url_link INT(11) NOT NULL DEFAULT 0,
					redirect_flows_text VARCHAR(100),
					text_colour VARCHAR(10),
					css TEXT,
					last_update TIMESTAMP,
					UNIQUE KEY ref (ref)
				) $charset_collate;";
				require_once(ABSPATH.'wp-admin/includes/upgrade.php');
				dbDelta($mysql);

				$mysql = "CREATE TABLE $obr_gcp_current_redirect_flows_table (
					ref INT(11) NOT NULL AUTO_INCREMENT,
					success_redirect_url_link INT(11) NOT NULL DEFAULT 0,
					session_token VARCHAR(100),
					full_session_token VARCHAR(100),
					redirect_id VARCHAR(100),
					last_update TIMESTAMP,
					UNIQUE KEY ref (ref)
				) $charset_collate;";
				require_once(ABSPATH.'wp-admin/includes/upgrade.php');
				dbDelta($mysql);

				$mysql = "CREATE TABLE $obr_gcp_payments_plans_table (
					ref INT(11) NOT NULL AUTO_INCREMENT,
					type INT(11) NOT NULL DEFAULT 1,
					amount INT(11) NOT NULL DEFAULT 0,
					currency INT(11) NOT NULL DEFAULT 1,
					name VARCHAR(200) NOT NULL DEFAULT 'Plan Name',
					interval_unit VARCHAR(20) NOT NULL DEFAULT 'monthly',
					day_of_month INT(11) NOT NULL DEFAULT 1,
					paytcount INT(11) NOT NULL DEFAULT 0,
					oneoffamount INT(11) NOT NULL DEFAULT 0,
					last_update TIMESTAMP,
					UNIQUE KEY ref (ref)
				) $charset_collate;";
				require_once(ABSPATH.'wp-admin/includes/upgrade.php');
				dbDelta($mysql);

				if ($init != 'init'){
					echo '<div id="message" class="updated fade"><p><strong>';
					printf(__('GoCardless Pro from Outerbridge updated to version %s', 'gcp'), $this->obr_gocardless_pro_version);
					echo '</strong></p></div>';
				}
				update_option( "obr_gocardless_pro_version", $this->obr_gocardless_pro_version);
			}
		}
		
		function obr_internationalisation(){
			// willkommen tout le monde...
			load_plugin_textdomain('gcp', false, dirname(plugin_basename(__FILE__)).'/lang/');
		}

		function obr_wpsm(){
			// we need wp session manager, thanks to Eric Mann
			require_once 'lib/wpsm/wp-session-manager.php';
			$this->obr_wpsm_session();
		}

		function obr_wpsm_session($data = null){
			// set session data
			global $wp_session;
			$wp_session = WP_Session::get_instance();
			if (strlen($wp_session['session_token']) == 0){
				$wp_session['session_token'] = md5(date('YmdHis'));
				if (strlen($data) > 0){
					$wp_session['session_token'] .= ':'.$data;
				}
			}
			return $wp_session['session_token'];
		}

		function obr_wpsm_expiry(){
			// set the expiry period
			// 60*60 = 1 hour
			$expiryperiod = 60*60;
			return $expiryperiod;
		}

		function obr_load_classes(){
			require_once 'classes/payments-plans.php';
		}

		function obr_check_system_config(){
			// the GoCardless libraries require at least PHP5.5
			if (version_compare(phpversion(), '5.5', '<')){
				?>
				<div class="notice notice-error">
					<p><?php printf(__('GoCardless requires at least PHP version 5.5 to be installed on your server to operate correctly.  You only have version %s installed.  Please contact your host and request PHP version 5.5.  Do not proceed until this problem has been rectified.', 'gcp'), phpversion()); ?></p>
				</div>
				<?php
				return false;
			}

			require_once 'lib/gcp/vendor/autoload.php';

			// need to establish that Guzzle is installed
			if (!class_exists('GuzzleHttp\Client')){
				?>
				<div class="notice notice-error">
					<p><?php _e('Guzzle is not installed on your system.  You cannot use GoCardless Pro until this is rectified.  Please see the Help & Usage page for more information.', 'gcp'); ?></p>
				</div>
				<?php
				return false;
			}

			// everything is okay
			return true;
		}

		function obr_is_ssl_check(){
			if (!is_ssl()){
				?>
				<div class="notice notice-error">
					<p><?php _e('Your site MUST be using HTTPS when you switch to Live.  You are seeing this message because your site is not using SSL.  If you try to use GoCardless in a Live environment without SSL enabled, you and your customers will see an error message and you will not be able to use GoCardless.  Your site can use HTTP for testing in Sandbox mode.', 'gcp'); ?></p>
				</div>
				<?php
				return false;
			}
			return true;
		}

		function obr_gocardless_pro_initialise(){
			// is the server okay?
			$system = $this->obr_check_system_config();
			if ($system === false){
				wp_die('Unable to continue.');
				return false;
			}			
		}
		
		function obr_header(){
			// credit function
			echo "\n\n\n".'<!-- Using GoCardless Pro plugin from Outerbridge.  For more information please visit http://outerbridge.co/gocardless-pro/ -->'."\n\n\n";
		}

		function obr_enqueue(){
			// add any additional css that's been set up
			global $wpdb;
			global $obr_gcp_paylinks_table;
			global $obr_gcp_redirect_flows_table;

			wp_register_script('gcp', plugins_url('/gocardless-pro.js', __FILE__), array('jquery'), false, true);
			wp_enqueue_script('gcp');
			wp_localize_script('gcp', 'gcpAjax', array(
														'ajaxurl' => admin_url('admin-ajax.php'),
														'ajaxnonce' => wp_create_nonce('gcpajax-nonce')
													));

			wp_enqueue_style('gcp-custom-style', plugins_url('/gocardless-pro.css', __FILE__));

			$paylinks = $wpdb->get_results("SELECT * FROM $obr_gcp_paylinks_table ORDER BY ref;");
			if (count($paylinks) > 0){
				$customcss = '';
				foreach ($paylinks as $paylink){
					$css = esc_textarea($paylink->css);
					$textcolour = esc_attr($paylink->text_colour);
					$customcss .= 'a.gcp_paylink'.intval($paylink->ref).'{ ';
					if (strlen($css) > 0){
						$customcss .= ' '.$css.' ';
					}
					if (strlen($textcolour) > 0){
						$customcss .= 'color: '.$textcolour.'; ';
					}
					$customcss .= ' } ';
				}
				if (strlen($customcss) > 0){
					wp_add_inline_style('gcp-custom-style', $customcss);
				}
			}

			$redirect_flows = $wpdb->get_results("SELECT * FROM $obr_gcp_redirect_flows_table ORDER BY ref;");
			if (count($redirect_flows) > 0){
				$customcss = '';
				foreach ($redirect_flows as $redirect_flow){
					$css = esc_textarea($redirect_flow->css);
					$textcolour = esc_attr($redirect_flow->text_colour);
					$customcss .= 'a.gcp_redirect_flow'.intval($redirect_flow->ref).'{ ';
					if (strlen($css) > 0){
						$customcss .= ' '.$css.' ';
					}
					if (strlen($textcolour) > 0){
						$customcss .= 'color: '.$textcolour.'; ';
					}
					$customcss .= ' } ';
				}
				if (strlen($customcss) > 0){
					wp_add_inline_style('gcp-custom-style', $customcss);
				}
			}
		}

		function obr_admin_enqueue($hook){
			wp_enqueue_style('gcp-custom-style-admin', plugins_url('/gocardless-pro-admin.css', __FILE__));

		    if(is_admin()){
		    	wp_enqueue_style('wp-color-picker');
		    	wp_enqueue_script('custom-script-handle', plugins_url('gocardless-pro-admin.js', __FILE__), array('wp-color-picker'), false, true);
		    }
		}

		function obr_gocardless_pro_admin_menu(){
			if (is_super_admin()){
				add_menu_page(__('GoCardless Pro Admin', 'gcp'), __('GoCardless Pro', 'gcp'), 'manage_options', 'obr-gocardless-pro', array($this, 'obr_gocardless_pro_account_set_up'), 'dashicons-vault', 80);
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro', 'gcp'), __('Account Set-Up', 'gcp'), 'manage_options', 'obr-gocardless-pro');
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Payments/Plans Set-Up', 'gcp'), __('Payments/Plans Set-Up', 'gcp'), 'manage_options', 'obr-gocardless-pro-payments-set-up', array($this, 'obr_gocardless_pro_payments_set_up'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Redirect Flows', 'gcp'), __('Redirect Flows', 'gcp'), 'manage_options', 'obr-gocardless-pro-redirect-flows', array($this, 'obr_gocardless_pro_redirect_flows'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Customers', 'gcp'), __('Customers', 'gcp'), 'manage_options', 'obr-gocardless-pro-customers', array($this, 'obr_gocardless_pro_customers'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Payments', 'gcp'), __('Payments', 'gcp'), 'manage_options', 'obr-gocardless-pro-payments', array($this, 'obr_gocardless_pro_payments'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Subscriptions', 'gcp'), __('Subscriptions', 'gcp'), 'manage_options', 'obr-gocardless-pro-subscriptions', array($this, 'obr_gocardless_pro_subscriptions'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro - Paylinks', 'gcp'), __('Paylinks', 'gcp'), 'manage_options', 'obr-gocardless-pro-paylinks', array($this, 'obr_gocardless_pro_paylinks'));
				add_submenu_page('obr-gocardless-pro', __('GoCardless Pro Admin - Help and Usage', 'gcp'), __('Help & Usage', 'gcp'), 'manage_options', 'obr-gocardless-pro-help-usage', array($this, 'obr_gocardless_pro_help_usage_admin'));
			}
		}

		function obr_error_messages(){
			global $errormessages;
			$returnstring = '';
			if (count($errormessages) > 0){
				$returnstring .= '<div class="gcperrors"><p>';
				$returnstring .= __('The following errors were encountered when contacting GoCardless\'s system:', 'gcp');
				$returnstring .= '</p>';
				$returnstring .= '<ol>';
				foreach ($errormessages as $errormessage){
					$returnstring .= '<li><pre>'.$errormessage.'</pre></li>';
				}
				$returnstring .= '</ol>';
			}
			return $returnstring;
		}

		function obr_gcp_activation(){
			wp_schedule_event(time(), 'hourly', 'obr_gcp_hourly_event');
		}

		function obr_rdf_transients_cleanup(){
			global $wpdb;
			global $obr_gcp_current_redirect_flows_table;

			$q = $wpdb->query("DELETE FROM $obr_gcp_current_redirect_flows_table WHERE `last_update` < (NOW() - INTERVAL 1 DAY);");
			return $q;
		}

		function obr_gcp_deactivation(){
			wp_clear_scheduled_hook('obr_gcp_hourly_event');
		}

		function obr_gocardless_pro_configure(){
			$accesstoken = get_option('obr_gcp_access_token');

			$accesstokenlive = get_option('obr_gcp_access_token_live');
			
			$systemstatus = get_option('obr_gcp_livesystem');

			if ($systemstatus == 'checked'){
				$config = array(
								'systemstatus' => $systemstatus,
								'accesstoken' => $accesstokenlive,
								'environment' => 'LIVE'
							);
			} else {
				$config = array(
								'systemstatus' => $systemstatus,
								'accesstoken' => $accesstoken,
								'environment' => 'SANDBOX'
							);
			}
			return $config;
		}

		function obr_gcp_api_call($systemstatus, $accesstoken, $type = 'customers', $action = 'list', $specific = null, $additional = null){
			//  main action method
			if ($accesstoken == '' || $accesstoken === null){
				return false;
			}

			global $errormessages;
			$okay = false;
			if ($systemstatus == 'checked'){
				try {
					$client = new \GoCardlessPro\Client(array(
						'access_token' => $accesstoken,
						'environment'  => \GoCardlessPro\Environment::LIVE
					));
					$okay = true;
				} catch (\GoCardlessPro\Core\Exception\ApiException $e){
					// Api request failed / record couldn't be created.
					$errormessages[] = __('Api request failed / record couldn\' be created.', 'gcp');
				} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e){
					// Unexpected non-JSON response
					$errormessages[] = __('Unexpected non-JSON response', 'gcp');
				} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e){
					// Network error
					$errormessages[] = __('Network error', 'gcp');
				}
			} else {
				try {
					$client = new \GoCardlessPro\Client(array(
						'access_token' => $accesstoken,
						'environment'  => \GoCardlessPro\Environment::SANDBOX
					));
					$okay = true;
				} catch (\GoCardlessPro\Core\Exception\ApiException $e){
					// Api request failed / record couldn't be created.
					$errormessages[] = __('Api request failed / record couldn\'t be created.', 'gcp');
				} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e){
					// Unexpected non-JSON response
					$errormessages[] = __('Unexpected non-JSON response', 'gcp');
				} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e){
					// Network error
					$errormessages[] = __('Network error', 'gcp');
				}
			}

			if ($okay === false){
				return false;
			}

			$gcpapi = false;
			if (in_array($type, array('customers', 'payments', 'mandates', 'customerBankAccounts', 'redirectFlows', 'subscriptions'), true)){
				if ($action == 'list'){
					try {
						$gcpapi = $client->$type()->list();
					} catch (Exception $e){
						$errormessages[] = sprintf(__('Error: %s', 'gcp'), $e);
					}
				}
				if ($action == 'get' && strlen($specific) > 0){
					try {
						$gcpapi = $client->$type()->get($specific);
					} catch (Exception $e){
						$errormessages[] = sprintf(__('Error: %s', 'gcp'), $e);
					}

				}
				if ($action == 'create' && $specific !== null){
					try {
						$gcpapi = $client->$type()->create($specific);
					} catch (Exception $e){
						$errormessages[] = sprintf(__('Error: %s', 'gcp'), $e);
					}
				}
				if ($action == 'complete' && $specific !== null && $type == 'redirectFlows'){
					try {
						$gcpapi = $client->$type()->complete($additional, $specific);
					} catch (Exception $e){
						$errormessages[] = sprintf(__('Error: %s', 'gcp'), $e);
					}

				}
				if ($action == 'cancel' && $specific !== null){
					try {
						$gcpapi = $client->$type()->cancel($specific);
					} catch (Exception $e){
						$errormessages[] = sprintf(__('Error: %s', 'gcp'), $e);
					}
				}
			}
			$errors = $this->obr_error_messages();
			if (strlen($errors) > 0){
				echo $errors;
			}
			return $gcpapi;
		}


		function obr_live_sandbox_status($systemstatus, $setuplink = true){
			// output the system status to the screen - for use on admin pages
			?>
			<div class="sandboxlivearea">
				<h3><?php _e('Live / Sandbox(Test) Mode', 'gcp'); ?></h3>
				<?php 
				if ($systemstatus == 'checked'){
					$colour = 'green';
					$status = __('LIVE', 'gcp');
				} else {
					$colour = 'red';
					$status = __('SANDBOX', 'gcp');
				} 
				?>
				<p style="padding: 10px; border: 3px solid <?php echo $colour; ?>; background-color: #f9f9f9;"><?php printf(__('You are working in <strong style="color: %s;">%s</strong> mode.', 'gcp'), $colour, $status); ?></p>
				<?php if ($setuplink) : ?>
					<p><?php printf(__('To switch modes go to the <a href="%s">Accounts Set-Up</a> tab.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro')); ?></p>
				<?php endif; ?>
			</div>
			<?php
		}

		function obr_gocardless_pro_account_set_up(){
			require_once 'gocardless-pro-account-set-up.php';
		}

		function obr_gocardless_pro_setup_options_update(){
			// validate and update the GoCardless options
			if (isset($_POST['livesystem']) && ($_POST['livesystem'] == 'on')){
				$display = 'checked';
			} else {
				$display = '';
			}
			update_option('obr_gcp_livesystem', $display);
			if (isset($_POST['access_token'])){
				update_option('obr_gcp_access_token', sanitize_text_field($_POST['access_token']));
			}
			if (isset($_POST['access_token_live'])){
				update_option('obr_gcp_access_token_live', sanitize_text_field($_POST['access_token_live']));
			}
		}
		
		function obr_gocardless_pro_payments_set_up(){
			require_once 'gocardless-pro-payments-set-up.php';
		}
		
		function obr_gocardless_pro_redirect_flows(){
			require_once 'gocardless-pro-redirect-flows.php';
		}

		function obr_gocardless_pro_customers(){
			require_once 'gocardless-pro-customers.php';
		}
		
		function obr_gocardless_pro_payments(){
			require_once 'gocardless-pro-payments.php';
		}
		
		function obr_gocardless_pro_subscriptions(){
			require_once 'gocardless-pro-subscriptions.php';
		}
		
		function obr_gocardless_pro_paylinks(){
			require_once 'gocardless-pro-paylinks.php';
		}

		function obr_gcp_paylink($atts, $content = null){
			global $wpdb;
			global $obr_gcp_paylinks_table;

			$returnstring = '';
			$atts = shortcode_atts(
							array(
								'ref' => 1
							), $atts, 'gcp_paylink'
						);
			$ref = $atts['ref'];
			$paylink = $wpdb->get_row("SELECT * FROM $obr_gcp_paylinks_table where ref='$ref';");
			if ($paylink){
				// the css is enqueued in the obr_enqueue method, not here
				$returnstring .= '<a href="'.$paylink->paylink_link.'" class="gcp_paylink gcp_paylink'.$ref.'">'.$paylink->paylink_text.'</a>';
			}


			return $returnstring.$content;
		}

		function obr_gcp_redirect_flow($atts, $content = null){
			global $wpdb;
			global $obr_gcp_redirect_flows_table;
			global $obr_gcp_payments_plans_table;

			$returnstring = '';
			extract(shortcode_atts(array(
										'ref' => 1, 
										'amount' => 1, 
										'onthefly' => 0
										), $atts));
			$ref = intval($atts['ref']);
			$redirect_flow = $wpdb->get_row("SELECT * FROM $obr_gcp_redirect_flows_table where ref='$ref';");
			if ($redirect_flow && $onthefly == 0){
				// check for NYOA
				$paymentplan = $wpdb->get_row("SELECT * FROM $obr_gcp_payments_plans_table where ref='{$redirect_flow->payment_plan}';");
				$type = intval($paymentplan->type);
				if ($type == 3){
					// we do have a NYOA so we need a form for it
					$currency = intval($paymentplan->currency);
					$returnstring = $this->obr_nyoa_form($ref, $currency);
					return $content.$returnstring;
				}
			}
			if ($redirect_flow){
				// the css is enqueued in the obr_enqueue method, not here
				$config = $this->obr_gocardless_pro_configure();
				$systemstatus = $config['systemstatus'];
				$accesstoken = $config['accesstoken'];
				if (!is_ssl() && $config['environment'] == 'LIVE'){
					$returnstring .= __('Unable to use GoCardless in Live on an HTTP website.', 'gcp');
				} else {
					global $wp_session;
					$sessiontoken = $wp_session['session_token'];

					$success_redirect_url = get_permalink(intval($redirect_flow->success_redirect_url_link));

					$description = esc_attr($wpdb->get_var("SELECT name FROM $obr_gcp_payments_plans_table where ref='{$redirect_flow->payment_plan}';"));
					if (strlen($description) == 0){
						$description = __('Set Up Mandate Only', 'gcp');
					} 

					$type = intval($wpdb->get_var("SELECT type FROM $obr_gcp_payments_plans_table where ref='{$redirect_flow->payment_plan}';"));
					if ($type == 3){
						// it's an NYOA so append the amount
						$description .= ' '.$amount/100;
					}

					$params = ['params' => ["description" => $description, "session_token" => $sessiontoken.':'.$ref.':'.$amount, "success_redirect_url" => $success_redirect_url]];
					if ( is_user_logged_in() ) {
						$current_user = wp_get_current_user();
						$params['params']["prefilled_customer"] = array(
							"given_name"  => $current_user->user_firstname,
							"family_name" => $current_user->user_lastname,
							"email"       => $current_user->user_email,
						);
					}
					$rdf = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'redirectFlows', 'create', $params);

					$q = $this->obr_rdf_transient_add($redirect_flow->success_redirect_url_link, $sessiontoken, $ref, $rdf->id, $amount);
					if ($q){
						$returnstring .= '<a href="'.$rdf->redirect_url.'" class="gcp_redirect_flow gcp_redirect_flow'.$ref.'">'.$redirect_flow->redirect_flows_text.'</a>';
					}
				}
			}
			if ($onthefly == 1){
				return $returnstring;
			}
			return $returnstring.$content;
		}

		function obr_complete_redirect_flows($content){
			global $wpdb;
			global $obr_gcp_current_redirect_flows_table;

			$returnstring = '';

			// first we need to decide whether YOAST SEO is active because it calls the_content twice before we actually need it
			// if YOAST SEO isn't active then we can plough on regardless

			$okay = false;
			if (defined('WPSEO_VERSION')){
				global $hereweareagain;
				$hereweareagain++;
				if ($hereweareagain == 3){
					// this is the third pass through the_content so we want to do our stuff this time
					$okay = true;
				}
			} else {
				// No Yoast SEO so we're okay to go
				$okay = true;
			}

			if(is_singular() && is_main_query() && $okay === true){
				$id = get_the_ID();
				$needed = intval($wpdb->get_var("SELECT COUNT(success_redirect_url_link) FROM $obr_gcp_current_redirect_flows_table WHERE success_redirect_url_link = '$id';"));
				if ($needed > 0){
					$config = $this->obr_gocardless_pro_configure();
					$systemstatus = $config['systemstatus'];
					$accesstoken = $config['accesstoken'];

					global $wp_session;
					$sessiontoken = $wp_session['session_token'];

					$error = false;
					if (isset($_GET['redirect_flow_id'])){

						$redirect_flow_id = esc_attr($_GET['redirect_flow_id']);

						$fullsessiontoken = esc_attr($wpdb->get_var("SELECT full_session_token FROM $obr_gcp_current_redirect_flows_table WHERE redirect_id = '$redirect_flow_id';"));

						if (strlen($fullsessiontoken) > 0){

							$params = ['params' => ["session_token" => $fullsessiontoken]];
							$rdf = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'redirectFlows', 'complete', $params, $redirect_flow_id);

							if ($rdf !== false){
								do_action('gcp_successful_mandate_setup', $rdf);
								$returnstring .= sprintf(__('<p>Set up mandate: %s</p>', 'gcp'), $rdf->links->mandate);

								// do we need to take a payment?
								$returnstring .= $this->obr_complete_setup_actions($rdf);

								$q = $this->obr_rdf_transient_delete($sessiontoken);
							} else {
								do_action('gcp_unsuccessful_mandate_setup', $redirect_flow_id);
							}

						} else {

							// we don't have a session token from the redirect_flow_id - probably it's been used before
							$message = sprintf(__('<p>Can\'t use redirect_flow_id: %s</p>', 'gcp'), $redirect_flow_id);
							$returnstring .= apply_filters('gcp_rdf_cant_use_redirect_flow_id', $message);

						}

					} else {

						// we don't have anything to process.  Have they come directly to the thank you page?
						$message = __('<p>Nothing to process</p>', 'gcp');
						$returnstring .= apply_filters('gcp_rdf_nothing_to_process', $message);

					}
				}
			}

			$content .= $returnstring;
			return $content;
		}

		function obr_complete_setup_actions($info){
			// temporary - dump this and the associated hook?

			global $wpdb;
			global $obr_gcp_payments_plans_table;
			global $obr_gcp_redirect_flows_table;

			$returnstring = '';

			$config = $this->obr_gocardless_pro_configure();
			$systemstatus = $config['systemstatus'];
			$accesstoken = $config['accesstoken'];

			$fullsessiontoken = $info->session_token;
			list($sessiontoken, $ref, $amount) = explode(':', $fullsessiontoken);
			$ref = intval($ref);
			$payment_plan = intval($wpdb->get_var("SELECT payment_plan FROM $obr_gcp_redirect_flows_table WHERE ref = '$ref';"));

			if($payment_plan == 0){
				// no payment required
				return false;
			}
			
			$selectq = "SELECT %s FROM $obr_gcp_payments_plans_table WHERE ref = '$payment_plan';";
			$currencyint = intval($wpdb->get_var(sprintf($selectq, 'currency')));
			$currency = $this->obr_gcp_currency_lookup($currencyint);
			$name = esc_attr($wpdb->get_var(sprintf($selectq, 'name')));
			$paytcount = intval($wpdb->get_var(sprintf($selectq, 'paytcount')));
			$oneoffamount = intval($wpdb->get_var(sprintf($selectq, 'oneoffamount')));
			
			$mandate = $info->links->mandate;

			$type = intval($wpdb->get_var(sprintf($selectq, 'type')));
			if ($type == 3){
				// this is a NYOA so adjust...
				$type = 1;
				// no need to adjust $amount as we already have it
			} else {
				$amount = intval($wpdb->get_var(sprintf($selectq, 'amount')));
			}
			switch ($type){
				case 1:
					// one-off payment
					$params = ['params' => ['amount' => $amount, 
											'currency' => $currency, 
											'description' => $name, 
											'links' => ['mandate' => $mandate]]];
					$setup = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'payments', 'create', $params);

					if ($setup){
						do_action('gcp_successful_payment_plan', $setup);
						$returnstring .= sprintf(__('<p>Set up payment: %1$s (%2$s) for %3$01.2f %4$s</p>', 'gcp'), $setup->id, $setup->description, $setup->amount/100, $setup->currency);
					} else {
						do_action('gcp_unsuccessful_payment_plan', $params);
					}
					break;
				
				case 2:
					// recurring payment plan
					$interval_unit = intval($wpdb->get_var(sprintf($selectq, 'interval_unit')));
					switch ($interval_unit){
						case 1:
							$interval_unit_wording = 'monthly';
							break;
						
						case 2:
							$interval_unit_wording = 'weekly';
							break;
						
						case 3:
							$interval_unit_wording = 'yearly';
							break;
						
						default:
							break;
					}
					$params = ['params' => ['amount' => $amount, 
											'currency' => $currency, 
											'name' => $name, 
											'interval_unit' => $interval_unit_wording, 
											'links' => ['mandate' => $mandate]]];
					$day_of_month = intval($wpdb->get_var(sprintf($selectq, 'day_of_month')));
					if ($day_of_month != 0 && $interval_unit_wording == 'monthly'){
						$params['params']['day_of_month'] = $day_of_month;
					}
					if ($paytcount != 0){
						// applies to all recurring interval types
						$params['params']['count'] = $paytcount;
					}
					$setup = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'subscriptions', 'create', $params);

					if ($setup){
						do_action('gcp_successful_payment_plan', $setup);
					} else {
						do_action('gcp_unsuccessful_payment_plan', $params);
					}

					if ($oneoffamount > 0){
						// we need to process the one-off amount separately...
						$oneoffparams = ['params' => ['amount' => $oneoffamount, 
													'currency' => $currency, 
													'description' => $name.__(' One-Off', 'gcp'), 
													'links' => ['mandate' => $mandate]]];
						$oneoffsetup = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'payments', 'create', $oneoffparams);

						if ($oneoffsetup){
							do_action('gcp_successful_oneoff_amount', $oneoffsetup);
						} else {
							do_action('gcp_unsuccessful_oneoff_amount', $oneoffparams);
						}

					}
					break;
				
				default:
					break;
			}

			return $returnstring;
		}

		function obr_rdf_transient_add($success_redirect_url_link = null, $session_token = null, $ref = null, $redirect_id = null, $amount = 0){
			if ($success_redirect_url_link == null || $session_token == null || $ref == null || $redirect_id == null){
				return false;
			}
			global $wpdb;
			global $obr_gcp_current_redirect_flows_table;
			$q = $wpdb->insert($obr_gcp_current_redirect_flows_table,
								array(	'success_redirect_url_link' => $success_redirect_url_link,
										'session_token' => $session_token,
										'full_session_token' => $session_token.':'.$ref.':'.$amount,
										'redirect_id' => $redirect_id
									),
								array(),
								array('%d', '%s', '%s', '%s')
							);
			return $q;
		}
		
		function obr_rdf_transient_delete($session_token = null){
			if ($session_token == null){
				return false;
			}
			global $wpdb;
			global $obr_gcp_current_redirect_flows_table;
			$q = $wpdb->delete($obr_gcp_current_redirect_flows_table,
								array('session_token' => $session_token),
								array('%s')
							);
			return $q;
		}

		function obr_gcp_currency_format($amount, $currency = 'GBP'){
			$returnstring = '';
			switch ($currency){
				case 'SEK':
					$returnstring = number_format($amount / 100, 2, ',', ' ').' kr';
					break;
				case 'EUR':
					$returnstring = '&euro; '.number_format($amount / 100, 2, ',', '.');
					break;
				case 'GBP':
					$returnstring = '&pound; '.number_format($amount / 100, 2, '.', ',');
					break;
				
				default:
					$returnstring = '&pound; '.number_format($amount / 100, 2, '.', ',');
					break;
			}
			return $returnstring;
		}

		function obr_gcp_currency_lookup($int = 3){
			$types = array(1 => 'GBP', 2 => 'EUR', 3 => 'SEK');
			if ($int > 0){
				return $types[$int];
			}
			return 'GBP';
		}

		function obr_gcp_select_dropdown($type = 'currency', $name = 'type', $selected = 1, $classname = null, $style = null){
			$returnstring = '';
			$returnstring .= '<select class="'.$classname.'" name="'.$name.'" style="'.$style.'">';
			switch ($type){
				case 'currency':
					$types = array(1 => 'GBP', 2 => 'EUR', 3 => 'SEK');
					break;
				case 'type':
					$types = array(1 => 'One-Off Payment', 2 => 'Recurring Payment Plan', 3 => 'Name Your Own Amount');
					break;
				case 'interval_unit':
					$types = array(1 => 'Monthly', 2 => 'Weekly', 3 => 'Yearly');
					break;
				case 'day_of_month':
					$types = array(0 => 'As Soon As Possible', 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14', 15 => '15', 16 => '16', 17 => '17', 18 => '18', 19 => '19', 20 => '20', 21 => '21', 22 => '22', 23 => '23', 24 => '24', 25 => '25', 26 => '26', 27 => '27', 28 => '28', -1 => 'Last Day Of Month');
					break;
				case 'paytcount':
					$types = array(0 => 'Forever', 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14', 15 => '15', 16 => '16', 17 => '17', 18 => '18', 19 => '19', 20 => '20', 21 => '21', 22 => '22', 23 => '23', 24 => '24', 25 => '25', 26 => '26', 27 => '27', 28 => '28', 29 => '29', 30 => '30', 31 => '31', 32 => '32', 33 => '33', 34 => '34', 35 => '35', 36 => '36');
					break;
				case 'payment_plan':
					global $wpdb;
					global $obr_gcp_payments_plans_table;
					$payments_plans = $wpdb->get_results("SELECT * FROM $obr_gcp_payments_plans_table ORDER BY ref;");
					$types = array();
					$types[0] = __('Set up mandate only', 'gcp');
					foreach ($payments_plans as $payment_plan){
						$types[$payment_plan->ref] = $payment_plan->name;
						$types[$payment_plan->ref] = $payment_plan->name.' (Payment Plan Ref '.$payment_plan->ref.')';
					}
					break;
				default:
					$types = array();
					break;
			}
			foreach ($types as $ref => $type){
				$returnstring .= '<option value="'.$ref.'"';
				if ($selected == $ref){
					$returnstring .= ' selected';
				}
				$returnstring .= '>'.$type.'</option>';
			}
			$returnstring .= '</select>';
			return $returnstring;
		}

		function obr_gocardless_pro_help_usage_admin(){
			require_once 'gocardless-pro-help-usage.php';
		}

		function obr_date($givendate = null, $format = 'd/m/Y H:i:s'){
			if ($givendate == null){
				$timenumber = current_time('timestamp');
			} else {
				$timenumber = strtotime($givendate);
			}
			$timenumber = $timenumber + (intval(get_option('gmt_offset', 0)) * 3600);
			$date = date($format, $timenumber);
			return $date;
		}

		function obr_nyoa_form($number, $currency = 1){
			// create a name your own amount form
			$ret = '';
			if (intval($number) > 0){
				$ret .= '<form><p>';
				$ret .= '<input id="amount" type="text" placeholder="'.__('Enter Amount', 'gc').'" />';
				$ret .= '<input type="hidden" id="linkref" value='.$number.' />';
				$ret .= '<input type="hidden" id="paymenttype" value=1 />';
				$ret .= '<input type="hidden" id="currency" value='.$currency.' />';
				$ret .= '<span id="paymentlink"></span>';
				$ret .= '</p></form>';
			}
			return $ret;
		}

		function obr_gcp_ajax_submit(){
			//get the submitted parameters
			$amount = floatval($_POST['amount']);
			$linkref = intval($_POST['linkref']);
			$paymenttype = intval($_POST['paymenttype']);
			$currency = esc_attr($_POST['currency']);

			$nonce = esc_attr($_POST['ajaxnonce']);
			if (!wp_verify_nonce($nonce, 'gcpajax-nonce')){
				die('Oh please!');
			}

			if ($amount == 0){
				$response['status'] = 'error';
				$response['message'] = __('No amount specified!', 'gc');
			} else {
				$amount *= 100;
				if ($amount > 500000){
					$amount = 500000;
				}
				$number = 2;

				$response['status'] = 'okay';
				$args = array(
							'amount' => $amount, 
							'ref' => $linkref,
							'onthefly' => 1,
							'currency' => $currency
						);
				$response['message'] = $this->obr_gcp_redirect_flow($args);
			}

			$response = json_encode($response);

			header("Content-Type: application/json");
			echo $response;

			// we MUST exit...
			exit;
		}
	}
	// set the new class
	$obr_gocardless_pro = new obr_gocardless_pro;
}

?>