jQuery(document).ready(function($){
	$("#amount, #paymenttype, #currency").on('input change', function(){
		var amount = $('#amount').val(),
			linkref = $('#linkref').val(),
			paymenttype = $('#paymenttype').val(),
			currency = $('#currency').val();
		$.post(
			gcpAjax.ajaxurl, 
			{
				action: 'gcp-ajax-submit',
				amount: amount,
				linkref: linkref,
				paymenttype: paymenttype,
				currency: currency,
				ajaxnonce: gcpAjax.ajaxnonce
			},
			function(response){
				//console.log(response);
				$('#paymentlink').html(response.message);
			}
		);
	});
});
