<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Payments', 'gcp'); ?></h2>

	<?php $config = $this->obr_gocardless_pro_configure();?>
	<?php $systemstatus = $config['systemstatus']; ?>
	<?php $this->obr_live_sandbox_status($systemstatus); ?>

	<h3><?php _e('Payments', 'gcp'); ?></h3>
	<p><?php _e('This is a list of your payments, and their status, in the GoCardless system.  This list is for information only; to administer your payments please use the GoCardless dashboard.', 'gcp'); ?></p>

	<?php
	$accesstoken = $config['accesstoken'];
	if (strlen($accesstoken) == 0){
		?>
		<p><?php _e('You need to supply your access tokens to be able to view this information.', 'gcp'); ?></p>
		<?php
		return false;
	}

	$payments = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'payments', 'list');
	if ($payments === false){
		?>
		<p><?php _e('We were unable to access any information.', 'gcp'); ?></p>
		<?php
		return false;
	}

		$count = 0;
		if (count($payments->records) > 0){
			?>
			<table class="wp-list-table widefat">
				<thead>
					<tr>
						<th><?php _e('No', 'gcp'); ?></th>
						<th><?php _e('GoCardless Id', 'gcp'); ?></th>
						<th><?php _e('Amount', 'gcp'); ?></th>
						<th><?php _e('Status', 'gcp'); ?></th>
						<th><?php _e('Customer', 'gcp'); ?></th>
						<th><?php _e('Description', 'gcp'); ?></th>
						<th><?php _e('Created', 'gcp'); ?></th>
						<th><?php _e('Charged', 'gcp'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($payments->records as $resource) : ?>
						<?php $count++; ?>
						<?php if ($count%2 == 0) : ?>
							<tr>
						<?php else : ?>
							<tr class="alternate">
						<?php endif; ?>
								<td><?php echo $count; ?></td>
								<td><?php echo $resource->id; ?></td>
								<td>
									<?php $formattedamount = $this->obr_gcp_currency_format($resource->amount, $resource->currency); ?>
									<?php echo $formattedamount; ?>
								</td>
								<td><?php echo str_replace('_', ' ', $resource->status); ?></td>
								<td>
									<?php $mandate = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'mandates', 'get', $resource->links->mandate); ?>
									<?php $cba = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'customerBankAccounts', 'get', $mandate->links->customer_bank_account); ?>
									<?php $customer = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'customers', 'get', $cba->links->customer); ?>
									<?php 
									echo '<a href="mailto:'.$customer->email.'">';
									if (((strlen($customer->given_name) > 0 || strlen($customer->family_name) > 0)) && (strlen($customer->company_name) > 0)){
										echo $customer->company_name.' ('.$customer->given_name.' '.$customer->family_name.')';
									} elseif (strlen($customer->given_name) > 0 || strlen($customer->family_name) > 0){
										echo $customer->given_name.' '.$customer->family_name;
									} elseif (strlen($customer->company_name) > 0){
										echo $customer->company_name;
									}
									echo '</a>';
									?>
								</td>
								<td><?php echo $resource->description; ?></td>
								<td><?php echo $this->obr_date($resource->created_at); ?></td>
								<td><?php echo date('d/m/Y', strtotime($resource->charge_date)); ?></td>
							</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php
		} else {
			?>

			<p><?php _e('There are no payment records.', 'gcp'); ?></p>
			<?php
		}
?>

</div>