<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Subscriptions', 'gcp'); ?></h2>

	<?php $config = $this->obr_gocardless_pro_configure();?>
	<?php $systemstatus = $config['systemstatus']; ?>
	<?php $this->obr_live_sandbox_status($systemstatus); ?>

	<h3><?php _e('Subscriptions', 'gcp'); ?></h3>
	<p><?php _e('This is a list of your subscriptions, and their status, in the GoCardless system.  This list is for information only; to administer your subscriptions please use the GoCardless dashboard.', 'gcp'); ?></p>

	<?php
	$accesstoken = $config['accesstoken'];
	if (strlen($accesstoken) == 0){
		?>
		<p><?php _e('You need to supply your access tokens to be able to view this information.', 'gcp'); ?></p>
		<?php
		return false;
	}

	if (isset($_POST['submit'])){
		// see whether we have any subscriptions to cancel before listing them
		$resourceid = esc_html($_POST['resourceid']);
		$deletedsubscription = false;
		if (strlen($resourceid) > 0){
			$deletedsubscription = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'subscriptions', 'cancel', $resourceid);
		}
		if ($deletedsubscription !== false){
			?>
			<div id="message" class="notice notice-success is-dismissible">
				<p><strong><?php printf(__('Subscription %s cancelled', 'gcp'), $resourceid); ?></strong></p>
			</div>
			<?php
		} else {
			?>
			<div id="message" class="notice notice-error">
				<p><strong><?php _e('We were unable to cancel the subscription', 'gcp'); ?></strong></p>
			</div>
			<?php
		}

	}

	// list our subscriptions
	$subscriptions = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'subscriptions', 'list');
	if ($subscriptions === false){
		?>
		<p><?php _e('We were unable to access any information.', 'gcp'); ?></p>
		<?php
		return false;
	}

		$count = 0;
		if (count($subscriptions->records) > 0){
			?>
			<table class="wp-list-table widefat">
				<thead>
					<tr>
						<th><?php _e('No', 'gcp'); ?></th>
						<th><?php _e('GoCardless Id', 'gcp'); ?></th>
						<th><?php _e('Amount', 'gcp'); ?></th>
						<th><?php _e('Status', 'gcp'); ?></th>
						<th><?php _e('Customer', 'gcp'); ?></th>
						<th><?php _e('Name', 'gcp'); ?></th>
						<th><?php _e('Created', 'gcp'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($subscriptions->records as $resource) : ?>
						<?php $count++; ?>
						<?php if ($count%2 == 0) : ?>
							<tr>
						<?php else : ?>
							<tr class="alternate">
						<?php endif; ?>
								<td><?php echo $count; ?></td>
								<td><?php echo $resource->id; ?></td>
								<td>
									<?php $formattedamount = $this->obr_gcp_currency_format($resource->amount, $resource->currency); ?>
									<?php echo $formattedamount; ?>
								</td>
								<td>
									<?php 
									echo str_replace('_', ' ', $resource->status); 
									if ($resource->status !== 'cancelled' && $resource->status !== 'finished'){
										?>
										<form method="POST" action="" onSubmit="if(!confirm('<?php printf(__('Are you sure that you want to delete subscription %s?', 'gcp'), $resource->id); ?>')){return false;}" class="inlineform">
											<input type="hidden" name="resourceid" value="<?php echo $resource->id; ?>" />
											<input type="submit" name="submit" class="button" value="<?php _e('Cancel', 'gcp'); ?>" />
										</form>
										<?php
									}
									?>
								</td>
								<td>
									<?php $mandate = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'mandates', 'get', $resource->links->mandate); ?>
									<?php $cba = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'customerBankAccounts', 'get', $mandate->links->customer_bank_account); ?>
									<?php $customer = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'customers', 'get', $cba->links->customer); ?>
									<?php 
									echo '<a href="mailto:'.$customer->email.'">';
									if (((strlen($customer->given_name) > 0 || strlen($customer->family_name) > 0)) && (strlen($customer->company_name) > 0)){
										echo $customer->company_name.' ('.$customer->given_name.' '.$customer->family_name.')';
									} elseif (strlen($customer->given_name) > 0 || strlen($customer->family_name) > 0){
										echo $customer->given_name.' '.$customer->family_name;
									} elseif (strlen($customer->company_name) > 0){
										echo $customer->company_name;
									}
									echo '</a>';
									?>
								</td>
								<td>
									<?php echo $resource->name; ?>
									<?php if (isset($resource->links->plan)) : ?>
										&nbsp;(<?php echo $resource->links->plan; ?>)
									<?php endif; ?>
								</td>
								<td><?php echo $this->obr_date($resource->created_at); ?></td>
							</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php
		} else {
			?>

			<p><?php _e('There are no subscription records.', 'gcp'); ?></p>
			<?php
		}
?>

</div>