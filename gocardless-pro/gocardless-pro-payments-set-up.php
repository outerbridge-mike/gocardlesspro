<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Payments/Plans Set-Up', 'gcp'); ?></h2>

	<?php $config = $this->obr_gocardless_pro_configure();?>
	<?php $systemstatus = $config['systemstatus']; ?>
	<?php $this->obr_live_sandbox_status($systemstatus); ?>

<?php
global $wpdb;
global $obr_gcp_payments_plans_table;
$message = '';
if (isset($_POST['addnew'])){
	// need to check and save the changes
	$okay = true;
	$type = intval($_POST['type'][0]);
	$amount = intval($_POST['amount'] * 100);
	if ($amount <= 0){
		$amount = 100;
		$message = __('However, the amount that you entered was invalid so a default amount of 1 has been used.  Please check and amend this before proceeding.', 'gcp');
	}
	$currency = intval($_POST['currency'][0]);
	$name = esc_attr($_POST['name']);
	$interval_unit = esc_attr($_POST['interval_unit'][0]);
	$day_of_month = intval($_POST['day_of_month'][0]);
	$paytcount = intval($_POST['paytcount'][0]);
	$oneoffamount = intval($_POST['oneoffamount'] * 100);
	$q = $wpdb->insert($obr_gcp_payments_plans_table,
						array(	'type' => $type,
								'amount' => $amount,
								'currency' => $currency,
								'name' => $name,
								'interval_unit' => $interval_unit,
								'day_of_month' => $day_of_month,
								'paytcount' => $paytcount,
								'oneoffamount' => $oneoffamount,
							),
						array('%d', '%d', '%d', '%s', '%s', '%d', '%d', '%d')
					);
	if ($q === null){
		$okay = false;
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Payment or Plan Added.', 'gcp'); ?></strong></p>
		</div>
		<?php if (strlen($message) > 0) : ?>
			<div id="message" class="error">
				<p><strong><?php echo $message; ?></strong></p>
			</div>
		<?php endif; ?>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Payment or Plan Could Not Be Added.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
if (isset($_POST['submit'])){
	// need to check and save the changes
	$okay = true;
	foreach ($_POST['ref'] as $ref){
		$ref = intval($ref);
		if (isset($_POST['delete'][$ref]) && esc_attr($_POST['delete'][$ref]) == 'on'){
			$q = $wpdb->delete($obr_gcp_payments_plans_table, array('ref' => $ref), array('%d'));
		} else {
			$type = intval($_POST['type'][$ref]);
			$amount = intval($_POST['amount'][$ref] * 100);
			if ($amount <= 0){
				$amount = 100;
			$message = __('However, one or more of the amounts that you entered was invalid so a default amount of 1 has been used.  Please check and amend your amounts before proceeding.', 'gcp');
			}
			$currency = intval($_POST['currency'][$ref]);
			$name = esc_attr($_POST['name'][$ref]);
			$interval_unit = esc_attr($_POST['interval_unit'][$ref]);
			$day_of_month = intval($_POST['day_of_month'][$ref]);
			$paytcount = intval($_POST['paytcount'][$ref]);
			$oneoffamount = intval($_POST['oneoffamount'][$ref] * 100);
			$q = $wpdb->update($obr_gcp_payments_plans_table,
								array(	'type' => $type,
										'amount' => $amount,
										'currency' => $currency,
										'name' => $name,
										'interval_unit' => $interval_unit,
										'day_of_month' => $day_of_month,
										'paytcount' => $paytcount,
										'oneoffamount' => $oneoffamount,
									),
								array('ref' => $ref),
								array('%d', '%d', '%d', '%s', '%s', '%d', '%d', '%d'),
								array('%d')
							);
		}
		if ($q === null){
			$okay = false;
		}
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Payment or Plan Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php if (strlen($message) > 0) : ?>
			<div id="message" class="error">
				<p><strong><?php echo $message; ?></strong></p>
			</div>
		<?php endif; ?>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Payment or Plan Could Not Be Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
?>
	<form method="POST" action="">
		<h3><?php _e('Payment and Payment Plan Set-Up', 'gcp'); ?></h3>
		<h4><?php _e('Existing Payments and Payment Plans', 'gcp'); ?></h4>
		<?php
		$payments_plans = $wpdb->get_results("SELECT * FROM $obr_gcp_payments_plans_table ORDER BY ref;");
		if (count($payments_plans) > 0) :
			$count = 0;
			?>

			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Ref', 'gcp'); ?></th>
						<th><?php _e('Type (Notes 1 & 2)', 'gcp'); ?></th>
						<th><?php _e('Amount (Note 3)', 'gcp'); ?></th>
						<th><?php _e('Currency (Note 4)', 'gcp'); ?></th>
						<th><?php _e('Name (Note 5)', 'gcp'); ?></th>
						<th><?php _e('Interval (Note 6)', 'gcp'); ?></th>
						<th><?php _e('Day of Month (Note 7)', 'gcp'); ?></th>
						<th><?php _e('Count (Note 8)', 'gcp'); ?></th>
						<th><?php _e('One-Off Amount (Note 9)', 'gcp'); ?></th>
						<th><?php _e('Delete Payment/Plan', 'gcp'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($payments_plans as $payment_plan) : ?>
						<?php if ($count%2 == 0) : ?>
							<tr class="alternate">
						<?php else : ?>
							<tr>
						<?php endif; ?>
								<td><input type="hidden" name="ref[<?php echo $payment_plan->ref; ?>]" value="<?php echo $payment_plan->ref; ?>" /><?php echo $payment_plan->ref; ?></td>
								<td><?php echo $this->obr_gcp_select_dropdown('type', 'type['.$payment_plan->ref.']', $payment_plan->type, 'oneofffinder'); ?></td>
								<td>
									<?php 
									$style = '';
									if ($payment_plan->type == 3){
										// this is a NYOA so initially hide it
										$style = 'display: none;';
									}
									?>
									<input name="amount[<?php echo $payment_plan->ref; ?>]" value="<?php echo (intval($payment_plan->amount)/100); ?>" class="nyoafinder<?php echo $payment_plan->ref; ?>" style="<?php echo $style; ?>" />
								</td>
								<td><?php echo $this->obr_gcp_select_dropdown('currency', 'currency['.$payment_plan->ref.']', $payment_plan->currency); ?></td>
								<td>
									<input name="name[<?php echo $payment_plan->ref; ?>]" value="<?php echo $payment_plan->name; ?>" />
									<?php if (strlen($payment_plan->name) == 0) : ?>
										<p class="warning"><?php _e('You need to enter some text here.', 'gcp'); ?></p>
									<?php endif; ?>
								</td>
								<td>
									<?php 
									$style = '';
									if ($payment_plan->type == 1 || $payment_plan->type == 3){
										// this is a one-off payment or NYOA so initially hide it
										$style = 'display: none;';
									}
									echo $this->obr_gcp_select_dropdown('interval_unit', 'interval_unit['.$payment_plan->ref.']', $payment_plan->interval_unit, 'interval_unit interval_unit'.$payment_plan->ref.' oneoffclass'.$payment_plan->ref, $style); 
									?>
								</td>
								<td>
									<?php
									$style = '';
									if ($payment_plan->type == 1 || $payment_plan->type == 3 || $payment_plan->interval_unit != 1){
										// this is a one-off payment or NYOA or not monthly recurring payment so initially hide it
										$style = 'display: none;';
									}
									echo $this->obr_gcp_select_dropdown('day_of_month', 'day_of_month['.$payment_plan->ref.']', $payment_plan->day_of_month, 'day_of_month'.$payment_plan->ref.' oneoffclass'.$payment_plan->ref, $style);
									?>
								</td>
								<td>
									<?php
									$style = '';
									if ($payment_plan->type == 1 || $payment_plan->type == 3){
										// this is a one-off payment or NYOA so initially hide it
										$style = 'display: none;';
									}
									echo $this->obr_gcp_select_dropdown('paytcount', 'paytcount['.$payment_plan->ref.']', $payment_plan->paytcount, 'paytcount'.$payment_plan->ref.' oneoffclass'.$payment_plan->ref, $style);
									?>
								</td>
								<td>
									<?php
									$style = '';
									if ($payment_plan->type == 1 || $payment_plan->type == 3){
										// this is a one-off payment or NYOA so initially hide it
										$style = 'display: none;';
									}
									?>
									<input name="oneoffamount[<?php echo $payment_plan->ref; ?>]" value="<?php echo (intval($payment_plan->oneoffamount)/100); ?>" class="oneoffamount<?php echo $payment_plan->ref; ?> oneoffclass<?php echo $payment_plan->ref; ?>" style="<?php echo $style; ?>" />
								</td>
								<td><input type="checkbox" name="delete[<?php echo $payment_plan->ref; ?>]" class="deletecheckbox2" /></td>
							</tr>
						<?php $count++; ?>
					<?php endforeach; ?>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td colspan="10"><input type="submit" name="submit" id="submit" value="<?php _e('Save Changes', 'gcp'); ?>" class="button button-primary" /></td>
				</tbody>
			</table>
		<?php endif; ?>
	</form>
	<h4><?php _e('Add New Payment or Payment Plan', 'gcp'); ?></h4>
	<form method="POST" action="">
			<?php $count = 0; ?>
			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Type (Notes 1 & 2)', 'gcp'); ?></th>
						<th><?php _e('Amount (Note 3)', 'gcp'); ?></th>
						<th><?php _e('Currency (Note 4)', 'gcp'); ?></th>
						<th><?php _e('Name (Note 5)', 'gcp'); ?></th>
						<th><?php _e('Interval (Note 6)', 'gcp'); ?></th>
						<th><?php _e('Day of Month (Note 7)', 'gcp'); ?></th>
						<th><?php _e('Count (Note 8)', 'gcp'); ?></th>
						<th><?php _e('One-Off Amount (Note 9)', 'gcp'); ?></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td><?php echo $this->obr_gcp_select_dropdown('type', 'type[0]', 2, 'oneofffinder'); ?></td>
							<td><input name="amount" class="nyoafinder0" /></td>
							<td><?php echo $this->obr_gcp_select_dropdown('currency', 'currency[0]'); ?></td>
							<td><input name="name" /></td>
							<td><?php echo $this->obr_gcp_select_dropdown('interval_unit', 'interval_unit[0]', 1, 'interval_unit interval_unit0 oneoffclass0'); ?></td>
							<td><?php echo $this->obr_gcp_select_dropdown('day_of_month', 'day_of_month[0]', 0, 'day_of_month0 oneoffclass0'); ?></td>
							<td><?php echo $this->obr_gcp_select_dropdown('paytcount', 'paytcount[0]', 0, 'paytcount0 oneoffclass0'); ?></td>
							<td><input name="oneoffamount" class="oneoffamount0 oneoffclass0" /></td>
							<td><input type="submit" name="addnew" id="addnew" value="<?php _e('Add New Payment or Plan', 'gcp'); ?>" class="button button-primary" /></td>
						</tr>
				</tbody>
			</table>
			<br />
			<h3>Notes:</h3>
			<ol>
				<li><?php printf(__('These one-off payments, recurring payment plans and name your own amount payments are used on <a href="%s">this page</a> to create the actual shortcodes that you can use on your site to take payments from your customers.  Choose whether you want your customer to make a single, set, one-off payment or to set them up for a recurring payment plan (subscription) or allow them to decide how much to pay as a one-off payment.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-redirect-flows')); ?></li>
				<li><?php _e('The Name Your Own Amount (NYOA) option allows your customers to choose how much they want to pay as a one-off payment.  Recurring payments are not yet included in NYOA functionality.', 'gcp'); ?></li>
				<li><?php _e('Enter the amount of the transction in the major currency unit.  For example, if you want &pound;123.45 enter 123.45 here; do not enter the currency unit.', 'gcp'); ?></li>
				<li><?php _e('Select the currency in which the payment or payment plan will take place.  Please note that you can only use currencies which have been enabled on your GoCardless account.', 'gcp'); ?></li>
				<li><?php _e('The name of the payment or payment plan that you enter here will be recorded in GoCardless as the description or plan name.', 'gcp'); ?></li>
				<li><?php _e('Recurring payment plans can take payments on a weekly, monthly or yearly basis.  Choose this here.', 'gcp'); ?></li>
				<li><?php printf(__('Monthly recurring payment plans can take payments on a specific day of the month.  You can choose any date between the 1st and the 28th, the last day of the month, or the default is to set the schedule up using the next available date in the GoCardless system.  Please note that GoCardless requires at least 3 working days to set up a new arrangement so bear this in mind when you set up your recurring payment plans.  For more information, please see <a href="%s" target="_blank">this page</a>.', 'gcp'), 'https://gocardless.com/faq/merchants/direct-debit/'); ?></li>
				<li><?php _e('All recurring payment plans can be set up to take payments forever, or you can choose to limit the number of payments.  For example, to set up an annual subscription taken each month during the year, you would choose a count of 12.', 'gcp'); ?></li>
				<li><?php _e('Enter any one-off amount to be charged at the start of a recurring payment plan using the same format as in Note 3 above.  This one-off amount is a separate transaction with GoCardless and is independent of the day of month setting (see Note 7 above), i.e. it is charged immediately.', 'gcp'); ?></li>
			</ol>
			<br />

	</form>
</div>
