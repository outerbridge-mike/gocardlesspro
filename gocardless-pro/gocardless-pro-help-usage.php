<div class="wrap">
	<?php 
	if (isset($_GET['tab'])){
		$activetab = esc_attr($_GET['tab']);
	} else {
		$activetab = 'intro';
	}
	?>
	<h2><?php _e('GoCardless Pro for WordPress - Help and Usage', 'gcp'); ?></h2>
	<h2 class="nav-tab-wrapper">
		<a href="?page=obr-gocardless-pro-help-usage&amp;tab=intro" class="nav-tab <?php echo $activetab == 'intro' ? 'nav-tab-active' : ''; ?>"><?php _e('Intro', 'gcp'); ?></a>
		<a href="?page=obr-gocardless-pro-help-usage&amp;tab=quickstart" class="nav-tab <?php echo $activetab == 'quickstart' ? 'nav-tab-active' : ''; ?>"><?php _e('Quickstart', 'gcp'); ?></a>
		<a href="?page=obr-gocardless-pro-help-usage&amp;tab=shortcodes" class="nav-tab <?php echo $activetab == 'shortcodes' ? 'nav-tab-active' : ''; ?>"><?php _e('Shortcodes', 'gcp'); ?></a>
		<a href="?page=obr-gocardless-pro-help-usage&amp;tab=video" class="nav-tab <?php echo $activetab == 'video' ? 'nav-tab-active' : ''; ?>"><?php _e('Video', 'gcp'); ?></a>
		<a href="?page=obr-gocardless-pro-help-usage&amp;tab=advanced" class="nav-tab <?php echo $activetab == 'advanced' ? 'nav-tab-active' : ''; ?>"><?php _e('Advanced', 'gcp'); ?></a>
	</h2>
	<?php if ($activetab == 'intro') : ?>
		<h3><?php _e('Introduction to GoCardless Pro for WordPress', 'gcp'); ?></h3>
		<br />
		<h4><?php _e('Intro', 'gcp'); ?></h4>
		<p><?php _e('GoCardless Pro for WordPress is a plugin which allows you to easily integrate the GoCardless low-cost 1% payment system with your WordPress website.', 'gcp'); ?></p>
		<p><?php printf(__('<strong style="color: red;">GoCardless Pro for WordPress uses the new, more powerful <a href="%s" target="_blank">GoCardless Pro API</a>.</strong>  Please contact GoCardless directly if you do not know which version of the API you are using.  Please note that if you use 4 API keys to access your GoCardless account (namely "App identifier", "App Secret", "Merchant Access Token" and "Merchant ID"), then you are using the Legacy API and will need to use <a href="%s">this plugin</a> or to contact GoCardless direct and ask to use the Pro API.)', 'gcp'), 'https://developer.gocardless.com/pro', 'https://codecanyon.net/item/gocardless-for-wordpress-plugin/3207246?ref=outerbridge'); ?></p>
		<p><?php _e('The GoCardless Pro plugin is implemented by adding a shortcode which creates a payment link to GoCardless Pro.  Shortcodes are special WordPress instructions encased within square brackets, e.g. &#0091;gcp_redirect_flow ref=1&#0093;, that can be used on pages and/or posts to make some complicated things happen.', 'gcp'); ?></p>
		<br />
		<h4><?php _e('New to / Upgrading GoCardless Pro?', 'gcp'); ?></h4>
		<p><?php _e('Follow the instructions on the Quickstart tab to get GoCardless Pro for WordPress up and running quickly.', 'gcp'); ?></p>
		<br />
		<h4><?php _e('Need More Help?', 'gcp'); ?></h4>
		<ol>
			<li><?php _e('We do not provide post-sales support for this plugin.  If, having thoroughly read the documentation, you are still unsure and need further assistance, please contact your web developer.', 'gcp'); ?></li>
			<li><?php _e('If you are looking for a significant customisation of the plugin\'s functionality, please contact us via our CodeCanyon profile stating your exact requirements and budget.  Depending on workload, we may be able to help.', 'gcp'); ?></li>
			<li><?php _e('If you find a bug in the plugin, please let us know via our CodeCanyon profile and we will investigate the problem as soon as we are able.  Any fixes required will be issued by us updating the plugin on CodeCanyon.', 'gcp'); ?></li>
		</ol>
		<br />
		<h4><?php _e('Finally...', 'gcp'); ?></h4>
		<blockquote><?php _e('"I\'m terrified of switching the computer on because there are so many poems.", <cite>Roger McGough</cite>', 'gcp'); ?></blockquote>
		<br />
	<?php elseif ($activetab == 'quickstart') : ?>
		<h3><?php _e('New to GoCardless Pro?  Quickstart Guide', 'gcp'); ?></h3>
		<br />
		<h4><?php _e('Overview', 'gcp'); ?></h4>
		<p><?php _e('These are the key stages to setting up GoCardless Pro:', 'gcp'); ?></p>
		<ol>
			<li><a href="#installation"><?php _e('Installation', 'gcp'); ?></a></li>
			<li><a href="#generalsetup"><?php _e('General Set-Up', 'gcp'); ?></a></li>
			<li><a href="#setupshortcodes"><?php _e('Setting Up The Shortcodes', 'gcp'); ?></a></li>
			<li><a href="#testing"><?php _e('Testing/Go-Live', 'gcp'); ?></a></li>
			<li><a href="#information"><?php _e('Information', 'gcp'); ?></a></li>
			<li><a href="#customisation"><?php _e('Customisation', 'gcp'); ?></a></li>
		</ol>
		<br />
		<h4 id="installation"><?php _e('Installation', 'gcp'); ?></h4>
		<p><?php _e('To install GoCardless Pro, first download the plugin (as a zip file) from your Downloads area in CodeCanyon and save a copy on your hard drive.  Then select Add New from the Plugins menu on the WordPress dashboard.  Then select Upload followed by Browse.  When you have located and selected the zip file that you downloaded from CodeCanyon, click on the Install Now button.  When completed, select the Activate option and GoCardless Pro will be ready for configuration on your site.', 'gcp'); ?></p>
		<br />
		<h4 id="generalsetup"><?php _e('General Set-Up', 'gcp'); ?></h4>
		<p><?php _e('If you have installed the plugin correctly, there will be a GoCardless Pro menu in the WordPress dashboard.', 'gcp'); ?></p>
		<p><?php _e('There are then three steps to follow to set up GoCardless Pro.  To complete each of these steps, visit the relevant pages in order.  Detailed guidance notes are provided at the bottom of each of the pages.', 'gcp'); ?></p>
		<ol>
			<li><?php printf(__('Click on the <a href="%s">Account Set-Up</a> page and enter the necessary details.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro')); ?></li>
			<li><?php printf(__('Go to the <a href="%s">Payments/Plans Set-Up</a> page and set up the one-off payments and recurring payment plans which you will require for your customers.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-payments-set-up')); ?></li>
			<li><?php printf(__('Finally go to the <a href="%s">Redirect Flows</a> page and set up and style the links that will be used on your website\'s pages.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-redirect-flows')); ?></li>
		</ol>
		<br />
		<h4 id="setupshortcodes"><?php _e('Setting Up The Shortcodes', 'gcp'); ?></h4>
		<p><?php printf(__('Go to the <a href="%s">Redirect Flows</a> page and copy the relevant shortcode from the "Shortcode" column.  Paste the shortcode, it takes the format [gcp_redirect_flow ref=X] (where X is the relevant reference number), on the page of your site that you want to use to take payments.  The plugin will then automatically create the GoCardless payment link for you.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-redirect-flows')); ?></p>
		<p><?php _e('For more information on the shortcodes available, and their usage, please visit the <a href="?page=obr-gocardless-pro-help-usage&amp;tab=shortcodes">Shortcodes</a> tab.', 'gcp'); ?></p>
		<br />
		<h4 id="testing"><?php _e('Testing/Go-Live', 'gcp'); ?></h4>
		<p><?php _e('Now is the time to perform detailed testing.  Before you set your site to Live for the purposes of taking payments you must ensure that everything is working correctly.', 'gcp'); ?></p>
		<p><?php printf(__('Only when you are happy that everything is operating as expected, switch to Live on the <a href="%s">Account Set-Up</a> page.  We recommend that you also check at least one Live transaction to ensure that payments are able to be processed correctly.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro')); ?></p>
		<p><?php _e('Your site MUST use HTTPS in the Live environment to be able to process transactions.', 'gcp'); ?></p>
		<br />
		<h4 id="information"><?php _e('Information', 'gcp'); ?></h4>
		<p><?php printf(__('Use the following links to see what information is stored at GoCardless about: <a href="%s">Customers</a>, <a href="%s">Payments</a> and <a href="%s">Subscriptions</a>.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-customers'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-payments'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-subscriptions')); ?></p>
		<br />
		<h4 id="customisation"><?php _e('Customisation', 'gcp'); ?></h4>
		<p><?php _e('Further customisation is available through the use of WordPress Action and Filter Hooks, please see the <a href="?page=obr-gocardless-pro-help-usage&amp;tab=shortcodes">Advanced</a> tab for more information.', 'gcp'); ?></p>
		<br />
	<?php elseif ($activetab == 'shortcodes') : ?>
		<h3><?php _e('Shortcodes', 'gcp'); ?></h3>
		<p><?php _e('GoCardless Pro is implemented via the shortcodes below, which you can copy and paste, or type, into a post or page.', 'gcp'); ?></p>
		<p><?php _e('Parameters are used to modify the default behaviour of the shortcode, for example, [gcp_redirect_flow ref=3] will display the third payment link that you have set up on the Redirect Flows page.  Parameters can be listed in any order after the shortcode name, but must be included before the ] symbol at the end of the shortcode.', 'gcp'); ?></p>
		<table class="wp-list-table widefat">
			<thead>
				<tr>
					<th><?php _e('Shortcode', 'gcp'); ?></th>
					<th><?php _e('Purpose', 'gcp'); ?></th>
					<th><?php _e('Usage', 'gcp'); ?></th>
					<th><?php _e('Parameters', 'gcp'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr class="alternate">
					<td>&#0091;gcp_redirect_flow&#0093;</td>
					<td>
						<p><?php _e('The &#0091;gcp_redirect_flow&#0093; shortcode is used to display a redirect flow link on a page.', 'gcp'); ?></p>
					</td>
					<td>
						<p><?php _e('Example: [gcp_redirect_flow ref=3] will display the third payment link that you have set up on the Redirect Flows page.', 'gcp'); ?></p>
					</td>
					<td>
						<p><?php _e('There is one parameter available: "ref".', 'gcp'); ?></p>
						<p><?php _e('Its use is optional.', 'gcp'); ?></p>
						<p><?php _e('The default value is 1.', 'gcp'); ?></p>
					</td>
				</tr>
				<tr>
					<td>&#0091;gcp_paylink&#0093;</td>
					<td>
						<p><?php _e('The &#0091;gcp_paylink&#0093; shortcode is used to display a gcp_paylink on a page.', 'gcp'); ?></p>
						<p><?php _e('Please note that Redirect Flows should be used in preference to Paylinks as they provide you with more control over the payments that you take. However, if you already have some paylinks set up in your GoCardless dashboard, you can use this shortcode to display them on your WordPress website.', 'gcp'); ?></p>
					</td>
					<td>
						<p><?php _e('Example: [gcp_paylink ref=3] will display the third payment paylink link that you have set up on the Paylinks page.', 'gcp'); ?></p>
					</td>
					<td>
						<p><?php _e('There is one parameter available: "ref".', 'gcp'); ?></p>
						<p><?php _e('Its use is optional.', 'gcp'); ?></p>
						<p><?php _e('The default value is 1.', 'gcp'); ?></p>
					</td>
				</tr>
			</tbody>
		</table>
	<?php elseif ($activetab == 'video') : ?>
		<h3><?php _e('Video', 'gcp'); ?></h3>
		<p><?php _e('A video showing you how easy it is to take a payment using GoCardless Pro.', 'gcp'); ?></p>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/t6KcSBI1NhE?rel=0" frameborder="0" allowfullscreen></iframe>
	<?php else : ?>
		<h3><?php _e('Action and Filter Hooks', 'gcp'); ?></h3>
		<p><?php _e('Advanced users may wish to further customise their installation of GoCardless Pro.  To help you with this we\'ve added a helpful range of action and filter hooks.', 'gcp'); ?></p>
		<p><?php _e('WordPress hooks allow plugins like GoCardless to "hook" into the rest of WordPress; i.e. to call functions in your plugin at specific times, letting you add your own custom functions to the plugin.  GoCardless Pro\'s specific hooks are detailed in the table below.', 'gcp'); ?></p>
		<p><?php _e('To set up a hook, you will need to add a function calling the hook to your functions.php file in your active theme.  Here\'s an example which sends an email containing the data passed from a successful mandate set-up to the admin email address of the site:', 'gcp'); ?></p>
		<pre>
function confirmationvalid($input){
	// my customised function
	wp_mail(get_option('admin_email'), 'gcp_successful_mandate_setup example', json_encode($input));
}
add_action('gcp_successful_mandate_setup', 'confirmationvalid');
		</pre>
		<h3><?php _e('Available Hooks:', 'gcp'); ?></h3>
		<table class="wp-list-table widefat">
			<thead>
				<tr>
					<th><?php _e('Hook', 'gcp'); ?></th>
					<th><?php _e('Type', 'gcp'); ?></th>
					<th><?php _e('Data Passed', 'gcp'); ?></th>
					<th><?php _e('Description', 'gcp'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr class="alternate">
					<td>gcp_successful_mandate_setup</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed is exactly that returned via the GoCardless Pro API, without any further validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_successful_mandate_setup" action is executed immediately after a successful mandate is set-up using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
				<tr>
					<td>gcp_unsuccessful_mandate_setup</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed is the id of the redirect flow identified when trying to set up a mandate via the GoCardless Pro API, without any validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_unsuccessful_mandate_setup" action is executed immediately after an unsuccessful mandate set-up attempt using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
				<tr class="alternate">
					<td>gcp_successful_payment_plan</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed are exactly that returned via the GoCardless Pro API, without any further validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_successful_payment_plan" action is executed immediately after a successful one-off payment or payment plan is set-up using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
				<tr>
					<td>gcp_unsuccessful_payment_plan</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed are the parameters used unsuccessfully to attempt to set up a one-off payment or recurring payment plan via the GoCardless Pro API, without any validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_unsuccessful_payment_plan" action is executed immediately after an unsuccessful one-off payment or recurring payment plan set-up attempt using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
				<tr class="alternate">
					<td>gcp_successful_oneoff_amount</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed are exactly that returned via the GoCardless Pro API, without any further validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_successful_oneoff_amount" action is executed immediately after a successful one-off payment as part of a recurring payment plan is set-up using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
				<tr>
					<td>gcp_unsuccessful_oneoff_amount</td>
					<td><?php _e('Action', 'gcp'); ?></td>
					<td><?php _e('The data passed are the parameters used unsuccessfully to attempt to set up the one-off payment as part of a recurring payment plan via the GoCardless Pro API, without any validation.', 'gcp'); ?></td>
					<td><?php _e('The "gcp_unsuccessful_oneoff_amount" action is executed immediately after an unsuccessful one-off payment as part of a recurring payment plan set-up attempt using the GoCardless Pro API.', 'gcp'); ?></td>
				</tr>
			</tbody>
		</table>
		<p><?php printf(__('Learn more about <a href="%s" target="_blank">WordPress\' API here</a>.', 'gcp'), 'http://codex.wordpress.org/Plugin_API'); ?></p>
		<p><?php _e('Watch this space as we\'re planning on releasing more hooks in future releases.', 'gcp'); ?></p>
	<?php endif; ?>
</div>
