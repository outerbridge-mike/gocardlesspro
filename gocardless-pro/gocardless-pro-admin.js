jQuery(document).ready(function($){
	$(function(){
		$('.pickcolour').wpColorPicker();
	});
});

jQuery(document).ready(function($){
	$('.deletecheckbox').change(function(){
		if ($(this).prop('checked')){
			alert("This paylink will be deleted after you press \"Save Changes\"?");
		}
	});
	$('.deletecheckbox2').change(function(){
		if ($(this).prop('checked')){
			alert("This payment or plan will be deleted after you press \"Save Changes\"?");
		}
	});
});

jQuery(document).ready(function($){
	$('.oneofffinder').change(function(){
		var selectedvalue,
			selectedreference,
			excludeditem;
		selectedreference = parseInt($(this).attr('name').replace(/[^0-9\.]/g, ''), 10);
		selectedvalue = $(this).val();
		if (selectedvalue == 1 || selectedvalue == 3){
			// it's a one-off payment or a NYOA
			$('.oneoffclass'+selectedreference).fadeOut();
		} else {
			// it's a recurring payment
			excludeditem = '';
			if ($('.interval_unit'+selectedreference).val() != 1){
				// it's not a monthly recurring item so exclude showing the day of the monthly
				excludeditem = '.day_of_month'+selectedreference;
			}
			$('.oneoffclass'+selectedreference).not(excludeditem).fadeIn();
		}
		if (selectedvalue == 3){
			// it's a NYOA
			$('.nyoafinder'+selectedreference).fadeOut();
		} else {
			// it's a not a NYOA
			$('.nyoafinder'+selectedreference).fadeIn();
		}
	});

	$('.interval_unit').change(function(){
		var selectedvalue,
			selectedreference;
		selectedreference = parseInt($(this).attr('name').replace(/[^0-9\.]/g, ''), 10);
		selectedvalue = $(this).val();
		if (selectedvalue == 1){
			// it's a monthly recurring payment
			$('.day_of_month'+selectedreference).fadeIn();
		} else {
			// it's not a monthly recurring payment
			$('.day_of_month'+selectedreference).fadeOut();
		}
	});
});

