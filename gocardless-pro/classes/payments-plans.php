<?php 

class gcp_payment_plan{
	public $id;
	public $type;
	public $type_description;
	public $amount;
	public $currency;
	public $name;
	public $interval_unit;
	public $day_of_month;
	public $paytcount;
	
	public function __construct($pp = null){
		global $wpdb;
		global $obr_gcp_payments_plans_table;

		$pp = intval($wpdb->get_var("SELECT MIN(ref) FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		if ($pp === null || $pp === false || $pp == 0){
			$pp = intval($wpdb->get_var("SELECT MIN(ref) FROM $obr_gcp_payments_plans_table;"));
		}
	
		$this->id = $pp;
		$this->getPaymentPlan($pp);
	}

	public function getPaymentPlan($pp){
		global $wpdb;
		global $obr_gcp_payments_plans_table;

		$amount = intval($wpdb->get_var("SELECT amount FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		$this->amount = $amount;
		
		$type = intval($wpdb->get_var("SELECT type FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		$this->type = $type;

		if ($type == 1){
			$this->type_description = __('One-Off Payment', 'gcp');
		} else {
			$this->type_description = __('Recurring Payment', 'gcp');
		}

		$currencyint = intval($wpdb->get_var("SELECT currency FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		$currency = $this->obr_gcp_lookup('currency', $currencyint);
		$this->currency = $currency;

		$name = esc_attr($wpdb->get_var("SELECT name FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		$this->name = $name;

		$interval_unitint = intval($wpdb->get_var("SELECT interval_unit FROM $obr_gcp_payments_plans_table WHERE ref = '$pp';"));
		$interval_unit = $this->obr_gcp_lookup('interval_unit', $interval_unitint);
		$this->interval_unit = $interval_unit;

	}

	public function obr_gcp_lookup($type = 'currency', $int = 1){
		if ($type == 'currency'){
			$types = array(1 => 'GBP', 2 => 'EUR', 3 => 'SEK');
			$returnstring = 'GBP';
		} elseif($type == 'interval_unit') {
			$types = array(1 => 'monthly', 2 => 'weekly', 3 => 'yearly');
			$returnstring = 'monthly';
		}

		if (strlen($types[$int]) > 0){
			$returnstring = $types[$int];
		}
		return $returnstring;
	}
}

?>