<?php 
if (isset($_POST['submit'])){
	// check to see whether we're updating the settings
	$this->obr_gocardless_pro_setup_options_update();
	?>
	<div id="message" class="updated">
		<p><strong><?php _e('Mode and Settings Saved.', 'gcp'); ?></strong></p>
	</div>
	<?php
}
?>
<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Account Set-Up', 'gcp'); ?></h2>
	<form method="POST" action="">
		<?php $config = $this->obr_gocardless_pro_configure();?>
		<?php $systemstatus = $config['systemstatus']; ?>
		<?php $this->obr_live_sandbox_status($systemstatus, false); ?>

		<h3><?php _e('GoCardless Account Set-Up', 'gcp'); ?></h3>
		<?php $accesstoken = get_option('obr_gcp_access_token'); ?>
		<?php $accesstokenlive = get_option('obr_gcp_access_token_live'); ?>

		<table summary="sandbox details" class="wp-list-table widefat">
			<thead>
				<tr>
					<th><?php _e('Item', 'gcp'); ?></th>
					<th><?php _e('Setting', 'gcp'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr class="alternate">
					<td><?php _e('Live System Enabled', 'gcp'); ?></td>
					<td><input type="checkbox" name="livesystem" id="livesystem" <?php echo get_option('obr_gcp_livesystem'); ?> /></td>
				</tr>
				<tr>
					<td>
						<?php _e('Sandbox Access token', 'gcp'); ?>
						<?php if (strlen(get_option('obr_gcp_livesystem') == 0) && strlen($accesstoken) == 0) : ?>
							<strong style="color: red;">
								<?php _e(' - You must enter a Sandbox Access token to be able to contact the GoCardless Pro API', 'gcp'); ?>
							</strong>
						<?php endif; ?>
					</td>
					<td><input type="text" style="width: 100%;" name="access_token" value="<?php echo $accesstoken; ?>" /></td>
				</tr>
				<tr class="alternate">
					<td>
						<?php _e('Live Access token', 'gcp'); ?>
						<?php if (get_option('obr_gcp_livesystem') == 'checked' && strlen($accesstokenlive) == 0) : ?>
							<strong style="color: red;">
								<?php _e(' - You must enter a Live Access token to be able to contact the GoCardless Pro API', 'gcp'); ?>
							</strong>
						<?php endif; ?>
					</td>
					<td><input type="text" style="width: 100%;" name="access_token_live" value="<?php echo $accesstokenlive; ?>" /></td>
				</tr>
			</tbody>
		</table>
		<br />
		<h3>Notes:</h3>
		<ol>
			<li><?php _e('You can find your Access Tokens in the Developer section of your GoCardless dashboard.  Create an access token with Read Write permissions and paste the token in the space above.  You will need to create two separate tokens; one in each of the Live and Sandbox sections of your GoCardless account.', 'gcp'); ?></li>
			<li><?php _e('Ensure you are happy with your settings by testing the system in Sandbox mode.  Uncheck the "Live System Enabled" box for Sandbox mode.', 'gcp'); ?></li>
			<li><?php _e('Your site must be using HTTPS when you switch to Live. If you try to use GoCardless in a Live environment without SSL enabled, you and your customers will see an error message and you will not be able to use GoCardless.  Your site can use HTTP for testing in Sandbox mode.', 'gcp'); ?></li>
		</ol>
		<br />

		<p><input type="submit" name="submit" id="submit" value="<?php _e('Save Changes', 'gcp'); ?>" class="button button-primary" /></p>

	</form>
</div>
