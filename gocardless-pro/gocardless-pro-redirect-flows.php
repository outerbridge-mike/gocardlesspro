<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Redirect Flows', 'gcp'); ?></h2>

	<?php $config = $this->obr_gocardless_pro_configure();?>
	<?php $systemstatus = $config['systemstatus']; ?>
	<?php $this->obr_live_sandbox_status($systemstatus); ?>

<?php
global $wpdb;
global $obr_gcp_redirect_flows_table;
if (isset($_POST['addnew'])){
	// need to check and save the changes
	$okay = true;
	$payment_plan = esc_attr($_POST['payment_plan']);
	$success_redirect_url_link = esc_attr($_POST['success_redirect_url_link']);
	$redirect_flows_text = esc_attr($_POST['redirect_flows_text']);
	$text_colour = esc_attr($_POST['text_colour']);
	$redirect_flow_css = esc_textarea($_POST['redirect_flow_css']);
	$q = $wpdb->insert($obr_gcp_redirect_flows_table,
						array(	'payment_plan' => $payment_plan,
								'success_redirect_url_link' => $success_redirect_url_link,
								'redirect_flows_text' => $redirect_flows_text,
								'text_colour' => $text_colour,
								'css' => $redirect_flow_css,
							),
						array('%s', '%s', '%s', '%s')
					);
	if ($q === null){
		$okay = false;
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Redirect Flows Added.', 'gcp'); ?></strong></p>
		</div>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Redirect Flows Could Not Be Added.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
if (isset($_POST['submit'])){
	// need to check and save the changes
	$okay = true;
	foreach ($_POST['ref'] as $ref){
		$ref = intval($ref);
		if (isset($_POST['delete'][$ref]) && esc_attr($_POST['delete'][$ref]) == 'on'){
			$q = $wpdb->delete($obr_gcp_redirect_flows_table, array('ref' => $ref), array('%d'));
		} else {
			$payment_plan = esc_attr($_POST['payment_plan'][$ref]);
			$success_redirect_url_link = esc_attr($_POST['success_redirect_url_link'][$ref]);
			$redirect_flows_text = esc_attr($_POST['redirect_flows_text'][$ref]);
			$text_colour = esc_attr($_POST['text_colour'][$ref]);
			$redirect_flow_css = esc_textarea($_POST['redirect_flow_css'][$ref]);
			$q = $wpdb->update($obr_gcp_redirect_flows_table,
								array(	'payment_plan' => $payment_plan,
										'success_redirect_url_link' => $success_redirect_url_link,
										'redirect_flows_text' => $redirect_flows_text,
										'text_colour' => $text_colour,
										'css' => $redirect_flow_css,
									),
								array('ref' => $ref),
								array('%d', '%d', '%s', '%s', '%s'),
								array('%d')
							);
		}
		if ($q === null){
			$okay = false;
		}
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Redirect Flows Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Redirect Flows Could Not Be Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
?>
	<form method="POST" action="">
		<h3><?php _e('Redirect Flows Set-Up', 'gcp'); ?></h3>
		<p><?php _e('Redirect Flows allow you to take payments (or set-up mandates to take payments at a later date) from your customers.', 'gcp'); ?></p>
		<?php
		$redirect_flows = $wpdb->get_results("SELECT * FROM $obr_gcp_redirect_flows_table ORDER BY ref;");
		if (count($redirect_flows) > 0) :
			$count = 0;
			?>

			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Ref', 'gcp'); ?></th>
						<th><?php _e('Shortcode (note 1)', 'gcp'); ?></th>
						<th><?php _e('Link to Payment Plan (note 2)', 'gcp'); ?></th>
						<th><?php _e('Success Redirect Page (note 3)', 'gcp'); ?></th>
						<th><?php _e('Redirect Flow Text (note 4)', 'gcp'); ?></th>
						<th><?php _e('Text Colour (note 5)', 'gcp'); ?></th>
						<th><?php _e('Additional CSS (note 6)', 'gcp'); ?></th>
						<th><?php _e('Delete Redirect Flow', 'gcp'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($redirect_flows as $redirect_flow) : ?>
						<?php if ($count%2 == 0) : ?>
							<tr class="alternate">
						<?php else : ?>
							<tr>
						<?php endif; ?>
								<td><input type="hidden" name="ref[<?php echo $redirect_flow->ref; ?>]" value="<?php echo $redirect_flow->ref; ?>" /><?php echo $redirect_flow->ref; ?></td>
								<td>[gcp_redirect_flow ref=<?php echo $redirect_flow->ref; ?>]</td>
								<td><?php echo $this->obr_gcp_select_dropdown('payment_plan', 'payment_plan['.$redirect_flow->ref.']', $redirect_flow->payment_plan); ?></td>
								<td>
									<?php wp_dropdown_pages(
												array(	'name' => 'success_redirect_url_link['.$redirect_flow->ref.']', 
														'selected' => $redirect_flow->success_redirect_url_link
														)
													); ?>
									<?php if (get_post_status(intval($redirect_flow->success_redirect_url_link)) === false || get_post_status(intval($redirect_flow->success_redirect_url_link)) === 'trash') : ?>
										<p class="warning"><?php _e('This link is set to a non-existent page or one which has been deleted.  You need to select a page on the site where the customer will be returned after the payment has been completed.  You will be unable to take payments unless you rectify this problem.', 'gcp'); ?></p>
									<?php endif; ?>
								</td>
								<td>
									<input name="redirect_flows_text[<?php echo $redirect_flow->ref; ?>]" value="<?php echo $redirect_flow->redirect_flows_text; ?>" />
									<?php if (strlen($redirect_flow->redirect_flows_text) == 0) : ?>
										<p class="warning"><?php _e('You need to enter some text here otherwise the redirect flow link will be used as the text.', 'gcp'); ?></p>
									<?php endif; ?>
								</td>
								<td><input name="text_colour[<?php echo $redirect_flow->ref; ?>]" value="<?php echo $redirect_flow->text_colour; ?>" class="pickcolour" /></td>
								<td>
									<p>.gcp_redirect_flow<?php echo $redirect_flow->ref; ?> {</p>
									<textarea id="redirect_flow_css[<?php echo $redirect_flow->ref; ?>]" name="redirect_flow_css[<?php echo $redirect_flow->ref; ?>]"><?php echo $redirect_flow->css; ?></textarea>
									<p>}</p>
								</td>
								<td><input type="checkbox" name="delete[<?php echo $redirect_flow->ref; ?>]" class="deletecheckbox" /></td>
							</tr>
						<?php $count++; ?>
					<?php endforeach; ?>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td colspan="7"><input type="submit" name="submit" id="submit" value="<?php _e('Save Changes', 'gcp'); ?>" class="button button-primary" /></td>
				</tbody>
			</table>
			<br />
		<?php endif; ?>
	</form>
	<form method="POST" action="">
			<?php $count = 0; ?>
			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Link to Payment Plan (note 2)', 'gcp'); ?></th>
						<th><?php _e('Success Redirect Page (note 3)', 'gcp'); ?></th>
						<th><?php _e('Redirect Flow Text (note 4)', 'gcp'); ?></th>
						<th><?php _e('Text Colour (note 5)', 'gcp'); ?></th>
						<th><?php _e('Additional CSS (note 6)', 'gcp'); ?></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td><?php echo $this->obr_gcp_select_dropdown('payment_plan', 'payment_plan', 0); ?></td>
							<td><?php wp_dropdown_pages(
												array(	'name' => 'success_redirect_url_link' 
														)
													); ?></td>
							<td><input name="redirect_flows_text" /></td>
							<td><input name="text_colour" class="pickcolour" /></td>
							<td><textarea id="redirect_flow_css" name="redirect_flow_css"></textarea></td>
							<td><input type="submit" name="addnew" id="addnew" value="<?php _e('Add New Redirect Flow', 'gcp'); ?>" class="button button-primary" /></td>
						</tr>
				</tbody>
			</table>
			<br />
			<h3>Notes:</h3>
			<ol>
				<li><?php _e('Copy and paste this shortcode into your page or post and the redirect flow will appear as link which your customers can follow to make payments.', 'gcp'); ?></li>
				<li><?php printf(__('Select a one-off payment or recurring payment plan which will be set up after the customer\'s payment mandate has been created.  Payments and payment plans are set up on <a href="%s">this page</a>.  If you do not have any payments or payment plans set-up, then you can still use the redirect flow link to create a mandate which you can charge payments or payment plans to at a later date.', 'gcp'), esc_url(get_admin_url().'admin.php?page=obr-gocardless-pro-payments-set-up')); ?></li>
				<li><?php _e('After your customer has entered their details on GoCardless\' site, they will be returned to your site to complete the transaction.  Choose the page that you would like your customers to be redirected to when they return from the list below, e.g. a "Thank You" page that you have set up previously.  Do NOT set this to a page on which Redirect Flows are created otherwise you will get an error.', 'gcp'); ?></li>
				<li><?php _e('This is the text that will appear on your page, e.g. "Pay &pound;100 now".  It will not appear in your customer\'s record at GoCardless.', 'gcp'); ?></li>
				<li><?php _e('This is the colour of the Redirect Flow Text in RGB format, e.g. #000000.  Alternatively, pick a colour from the palette.', 'gcp'); ?></li>
				<li><?php _e('Optionally, you can put any additional CSS that you want here in the format: <strong>property: value;</strong>.  The specific class for this link is shown just above the input box.  Use "!important" with the "color" property to override the colour entered in the point above.', 'gcp'); ?></li>
			</ol>
			<br />


	</form>
</div>
