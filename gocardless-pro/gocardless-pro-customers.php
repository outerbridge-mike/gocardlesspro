<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Customers', 'gcp'); ?></h2>

	<?php $config = $this->obr_gocardless_pro_configure();?>
	<?php $systemstatus = $config['systemstatus']; ?>
	<?php $this->obr_live_sandbox_status($systemstatus); ?>

	<h3><?php _e('Customers', 'gcp'); ?></h3>
	<p><?php _e('This is a list of your customers in the GoCardless system.  This list is for information only; to administer your customers please use the GoCardless dashboard.', 'gcp'); ?></p>

	<?php
	$accesstoken = $config['accesstoken'];
	if (strlen($accesstoken) == 0){
		?>
		<p><?php _e('You need to supply your access tokens to be able to view this information.', 'gcp'); ?></p>
		<?php
		return false;
	}

	$customers = $this->obr_gcp_api_call($systemstatus, $accesstoken, 'customers', 'list');
	if ($customers === false){
		?>
		<p><?php _e('We were unable to access any information.', 'gcp'); ?></p>
		<?php
		return false;
	}

	$count = 0;
	if (count($customers->records) > 0){
		?>
		<table class="wp-list-table widefat">
			<thead>
				<tr>
					<th><?php _e('No', 'gcp'); ?></th>
					<th><?php _e('GoCardless Id', 'gcp'); ?></th>
					<th><?php _e('First Name', 'gcp'); ?></th>
					<th><?php _e('Surname', 'gcp'); ?></th>
					<th><?php _e('Company Name', 'gcp'); ?></th>
					<th><?php _e('Email', 'gcp'); ?></th>
					<th><?php _e('Created', 'gcp'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($customers->records as $resource) : ?>
					<?php $count++; ?>
					<?php if ($count%2 == 0) : ?>
						<tr>
					<?php else : ?>
						<tr class="alternate">
					<?php endif; ?>
							<td><?php echo $count; ?></td>
							<td><?php echo $resource->id; ?></td>
							<td><?php echo $resource->given_name; ?></td>
							<td><?php echo $resource->family_name; ?></td>
							<td><?php echo $resource->company_name; ?></td>
							<td><?php echo '<a href="mailto:'.$resource->email.'">'.$resource->email.'</a>'; ?></td>
							<td><?php echo $this->obr_date($resource->created_at); ?></td>
						</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	} else {
		?>

		<p><?php _e('There are no customer records.', 'gcp'); ?></p>
		<?php
	}
?>

</div>