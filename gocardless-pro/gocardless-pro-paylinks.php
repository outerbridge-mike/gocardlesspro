<?php
global $wpdb;
global $obr_gcp_paylinks_table;
if (isset($_POST['addnew'])){
	// need to check and save the changes
	$okay = true;
	$paylink_link = esc_attr($_POST['paylink_link']);
	$paylink_text = esc_attr($_POST['paylink_text']);
	$text_colour = esc_attr($_POST['text_colour']);
	$paylink_css = esc_textarea($_POST['paylink_css']);
	$q = $wpdb->insert($obr_gcp_paylinks_table,
						array(	'paylink_link' => $paylink_link,
								'paylink_text' => $paylink_text,
								'text_colour' => $text_colour,
								'css' => $paylink_css,
							),
						array('%s', '%s', '%s', '%s')
					);
	if ($q === null){
		$okay = false;
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Paylinks Added.', 'gcp'); ?></strong></p>
		</div>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Paylinks Could Not Be Added.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
if (isset($_POST['submit'])){
	// need to check and save the changes
	$okay = true;
	foreach ($_POST['ref'] as $ref){
		$ref = intval($ref);
		if (isset($_POST['delete'][$ref]) && esc_attr($_POST['delete'][$ref]) == 'on'){
			$q = $wpdb->delete($obr_gcp_paylinks_table, array('ref' => $ref), array('%d'));
		} else {
			$paylink_link = esc_attr($_POST['paylink_link'][$ref]);
			$paylink_text = esc_attr($_POST['paylink_text'][$ref]);
			$text_colour = esc_attr($_POST['text_colour'][$ref]);
			$paylink_css = esc_textarea($_POST['paylink_css'][$ref]);
			$q = $wpdb->update($obr_gcp_paylinks_table,
								array(	'paylink_link' => $paylink_link,
										'paylink_text' => $paylink_text,
										'text_colour' => $text_colour,
										'css' => $paylink_css,
									),
								array('ref' => $ref),
								array('%s', '%s', '%s', '%s'),
								array('%d')
							);
		}
		if ($q === null){
			$okay = false;
		}
	}
	if ($okay !== false){
		?>
		<div id="message" class="updated">
			<p><strong><?php _e('Paylinks Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php
	} else {
		?>
		<div id="message" class="error">
			<p><strong><?php _e('Paylinks Could Not Be Saved.', 'gcp'); ?></strong></p>
		</div>
		<?php
	}
}
?>
<div class="wrap">
	<h2><?php _e('GoCardless Pro for WordPress - Paylinks', 'gcp'); ?></h2>
	<form method="POST" action="">
		<h3><?php _e('Paylinks Set-Up', 'gcp'); ?></h3>
		<p><?php _e('<strong style="color: red;">Redirect Flows should be used in preference to Paylinks</strong> as they provide you with more control over the payments that you take.  However, if you already have some paylinks set up in your GoCardless dashboard, you can use them on your WordPress website by using this page.', 'gcp'); ?></p>
		<p><?php _e('Set up the paylinks in your GoCardless dashboard and then style them here.', 'gcp'); ?></p>
		<?php
		$paylinks = $wpdb->get_results("SELECT * FROM $obr_gcp_paylinks_table ORDER BY ref;");
		if (count($paylinks) > 0) :
			$count = 0;
			?>

			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Ref', 'gcp'); ?></th>
						<th><?php _e('Shortcode (note 1)', 'gcp'); ?></th>
						<th><?php _e('Paylink URL (note 2)', 'gcp'); ?></th>
						<th><?php _e('Paylink Text (note 3)', 'gcp'); ?></th>
						<th><?php _e('Text Colour (note 4)', 'gcp'); ?></th>
						<th><?php _e('Additional CSS (note 5)', 'gcp'); ?></th>
						<th><?php _e('Delete Paylink', 'gcp'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($paylinks as $paylink) : ?>
						<?php if ($count%2 == 0) : ?>
							<tr class="alternate">
						<?php else : ?>
							<tr>
						<?php endif; ?>
								<td><input type="hidden" name="ref[<?php echo $paylink->ref; ?>]" value="<?php echo $paylink->ref; ?>" /><?php echo $paylink->ref; ?></td>
								<td>[gcp_paylink ref=<?php echo $paylink->ref; ?>]</td>
								<td>
									<input name="paylink_link[<?php echo $paylink->ref; ?>]" value="<?php echo $paylink->paylink_link; ?>" />
									<?php if (strpos($paylink->paylink_link, 'https://') !== 0 && strpos($paylink->paylink_link, 'gocardless.com/') === false) : ?>
										<p class="warning"><?php _e('This link doesn\'t look right, please check it carefully.', 'gcp'); ?></p>
									<?php endif; ?>
								</td>
								<td>
									<input name="paylink_text[<?php echo $paylink->ref; ?>]" value="<?php echo $paylink->paylink_text; ?>" />
									<?php if (strlen($paylink->paylink_text) == 0) : ?>
										<p class="warning"><?php _e('You need to enter some text here otherwise the paylink link will be used as the text.', 'gcp'); ?></p>
									<?php endif; ?>
								</td>
								<td><input name="text_colour[<?php echo $paylink->ref; ?>]" value="<?php echo $paylink->text_colour; ?>" class="pickcolour" /></td>
								<td>
									<p>.gcp_paylink<?php echo $paylink->ref; ?> {</p>
									<textarea id="paylink_css[<?php echo $paylink->ref; ?>]" name="paylink_css[<?php echo $paylink->ref; ?>]"><?php echo $paylink->css; ?></textarea>
									<p>}</p>
								</td>
								<td><input type="checkbox" name="delete[<?php echo $paylink->ref; ?>]" class="deletecheckbox" /></td>
							</tr>
						<?php $count++; ?>
					<?php endforeach; ?>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td colspan="7"><input type="submit" name="submit" id="submit" value="<?php _e('Save Changes', 'gcp'); ?>" class="button button-primary" /></td>
				</tbody>
			</table>
			<br />
		<?php endif; ?>
	</form>
	<form method="POST" action="">
			<?php $count = 0; ?>
			<table class="wp-list-table widefat wideinputs">
				<thead>
					<tr>
						<th><?php _e('Paylink URL (note 2)', 'gcp'); ?></th>
						<th><?php _e('Paylink Text (note 3)', 'gcp'); ?></th>
						<th><?php _e('Text Colour (note 4)', 'gcp'); ?></th>
						<th><?php _e('Additional CSS (note 5)', 'gcp'); ?></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>

					<?php if ($count%2 == 0) : ?>
						<tr class="alternate">
					<?php else : ?>
						<tr>
					<?php endif; ?>
							<td><input name="paylink_link" /></td>
							<td><input name="paylink_text" /></td>
							<td><input name="text_colour" class="pickcolour" /></td>
							<td><textarea id="paylink_css" name="paylink_css"></textarea></td>
							<td><input type="submit" name="addnew" id="addnew" value="<?php _e('Add New Paylink', 'gcp'); ?>" class="button button-primary" /></td>
						</tr>
				</tbody>
			</table>
			<br />
			<h3>Notes:</h3>
			<ol>
				<li><?php _e('You need to copy and paste this shortcode into your page to make the paylink appear.', 'gcp'); ?></li>
				<li><?php printf(__('To get your paylink, you must log in to your GoCardless.com dashboard and then go to the "Payments" page and press the "Paylinks" button in the right-hand side of the page.  From there either create a new paylink or copy an existing one into the box provided here.  The format of the Paylink will be something like https://pay-sandbox.gocardless.com/AL00001A1111AA.  <strong>Do not forget to set the "Redirect URI" under the "Advanced options" or your customer will not be returned to your site when the transaction has been completed.</strong>  The Redirect URI will look something like %s.', 'gcp'), trailingslashit(get_home_url(null, 'thankyou'))); ?></li>
				<li><?php _e('This is the text that will appear on your page, e.g. "Pay &pound;100 now".  It will not appear in your customer\'s record at GoCardless.', 'gcp'); ?></li>
				<li><?php _e('This is the colour of the Paylink Text in RGB format, e.g. #000000.  Alternatively, pick a colour from the palette.', 'gcp'); ?></li>
				<li><?php _e('Optionally, you can put any additional CSS that you want here in the format: <strong>property: value;</strong>.  The specific class for this link is shown just above the input box.  Use "!important" with the "color" property to override the colour entered in the point above.', 'gcp'); ?></li>
			</ol>
			<br />


	</form>
</div>
